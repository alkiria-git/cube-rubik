/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rubik.model;

import java.io.Serializable;
import javafx.scene.paint.Color;

/**
 *
 * @author Eduardo Salas Cerdas
 */
public class Pieza implements Serializable{
    //Atributos
    private Color frente;
    private Color derecha;
    private Color atras;
    private Color izquierda;
    private Color arriba;
    private Color abajo;
    private String tipo;
    
    //Constructores
    public Pieza(){
        this.frente = null;
        this.derecha = null;
        this.atras = null;
        this.izquierda = null;
        this.arriba = null;
        this.abajo = null;
        this.tipo = "";
    }
    
    public Pieza(Color frente, Color derecha, Color atras, Color izquierda, Color arriba, Color abajo,  String tipo){
        this.frente = frente;
        this.derecha = derecha;
        this.atras = atras;
        this.izquierda = izquierda;
        this.arriba = arriba;
        this.abajo = abajo;
        this.tipo = tipo;
    }

    //Gets and Sets
    public Color getFrente() {
        return frente;
    }

    public void setFrente(Color frente) {
        this.frente = frente;
    }

    public Color getDerecha() {
        return derecha;
    }

    public void setDerecha(Color derecha) {
        this.derecha = derecha;
    }

    public Color getAtras() {
        return atras;
    }

    public void setAtras(Color atras) {
        this.atras = atras;
    }

    public Color getIzquierda() {
        return izquierda;
    }

    public void setIzquierda(Color izquierda) {
        this.izquierda = izquierda;
    }

    public Color getArriba() {
        return arriba;
    }

    public void setArriba(Color arriba) {
        this.arriba = arriba;
    }

    public Color getAbajo() {
        return abajo;
    }

    public void setAbajo(Color abajo) {
        this.abajo = abajo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
