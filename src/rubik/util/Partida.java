/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rubik.util;
import java.io.Serializable;

/**
 *
 * @author Eduardo Salas Cerdas
 */
public class Partida implements Serializable{
    //Atributos
    private String nombre;
    private String pasos;
    private String minutos;
    private String segundos;
    private String caraFrente[][];
    private String caraAtras[][];
    private String caraDerecha[][];
    private String caraIzquierda[][];
    private String caraArriba[][];
    private String caraAbajo[][];
    
    //Constructores
    public Partida(){
        this.nombre = "";
        this.pasos = "";
        this.minutos = "";
        this.segundos = "";
        this.caraFrente = new String[3][3];
        this.caraAtras = new String[3][3];
        this.caraDerecha = new String[3][3];
        this.caraIzquierda = new String[3][3];
        this.caraArriba = new String[3][3];
        this.caraAbajo = new String[3][3];
    }
    
    public Partida(String name, String steps, String minutes, String seconds,
            String[][] caraF, String[][] caraB, String[][] caraR, String[][] caraL, String[][] caraU, String[][] caraD){
        this();
        this.nombre = name;
        this.pasos = steps;
        this.minutos = minutes;
        this.segundos = seconds;
        this.caraFrente = caraF;
        this.caraAtras = caraB;
        this.caraDerecha = caraR;
        this.caraIzquierda = caraL;
        this.caraArriba = caraU;
        this.caraAbajo = caraD;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPasos() {
        return pasos;
    }

    public void setPasos(String pasos) {
        this.pasos = pasos;
    }

    public String getMinutos() {
        return minutos;
    }

    public void setMinutos(String minutos) {
        this.minutos = minutos;
    }

    public String getSegundos() {
        return segundos;
    }

    public void setSegundos(String segundos) {
        this.segundos = segundos;
    }

    public String[][] getCaraFrente() {
        return caraFrente;
    }

    public void setCaraFrente(String[][] caraFrente) {
        this.caraFrente = caraFrente;
    }

    public String[][] getCaraAtras() {
        return caraAtras;
    }

    public void setCaraAtras(String[][] caraAtras) {
        this.caraAtras = caraAtras;
    }

    public String[][] getCaraDerecha() {
        return caraDerecha;
    }

    public void setCaraDerecha(String[][] caraDerecha) {
        this.caraDerecha = caraDerecha;
    }

    public String[][] getCaraIzquierda() {
        return caraIzquierda;
    }

    public void setCaraIzquierda(String[][] caraIzquierda) {
        this.caraIzquierda = caraIzquierda;
    }

    public String[][] getCaraArriba() {
        return caraArriba;
    }

    public void setCaraArriba(String[][] caraArriba) {
        this.caraArriba = caraArriba;
    }

    public String[][] getCaraAbajo() {
        return caraAbajo;
    }

    public void setCaraAbajo(String[][] caraAbajo) {
        this.caraAbajo = caraAbajo;
    }
    
    
}
