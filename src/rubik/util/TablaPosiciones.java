/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rubik.util;

import java.util.ArrayList;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Eduardo Salas Cerdas
 */
public class TablaPosiciones {
    //Atributos
    private SimpleStringProperty nombre;
    private SimpleIntegerProperty puntaje;
    private SimpleStringProperty movimientos;
    private SimpleStringProperty tiempo;
    private ArrayList<TablaPosiciones> tabla;
    
    //Constructores
    public TablaPosiciones(){
        this.nombre = new SimpleStringProperty();
        this.puntaje = new SimpleIntegerProperty();
        this.movimientos = new SimpleStringProperty();
        this.tiempo = new SimpleStringProperty();
        this.tabla = new ArrayList();
        AppContext.getInstance().set("tp", this);
    }
    
    public TablaPosiciones(String nombre, Integer puntaje, String movimientos, String tiempo){
        this.nombre = new SimpleStringProperty(nombre);
        this.puntaje = new SimpleIntegerProperty(puntaje);
        this.movimientos = new SimpleStringProperty(movimientos);
        this.tiempo = new SimpleStringProperty(tiempo);
    }
    
    //Gets
    public String getNombre(){
        return this.nombre.getValue();
    }
    
    public Integer getPuntaje(){
        return this.puntaje.getValue();
    }
    
    public String getMovimientos(){
        return this.movimientos.getValue();
    }
    
    public String getTiempo(){
        return this.tiempo.getValue();
    }
    
    public ArrayList getTabla(){
        return this.tabla;
    }
    
    //Sets
    public void setNombre(String nombre){
        this.nombre = new SimpleStringProperty(nombre);
    }
    
    public void setPuntaje(Integer puntaje){
        this.puntaje = new SimpleIntegerProperty(puntaje);
    }
    
    public void setMovimientos(String movimientos){
        this.movimientos = new SimpleStringProperty(movimientos);
    }
    
    public void setTiempo(String tiempo){
        this.tiempo = new SimpleStringProperty(tiempo);
    }
}
