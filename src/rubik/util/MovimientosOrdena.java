/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rubik.util;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Eduardo Salas Cerdas
 */
public class MovimientosOrdena {
    //Atributo
    private SimpleStringProperty movimiento;
    
    //Constructor
    public MovimientosOrdena(){
        this.movimiento = new SimpleStringProperty();
    }
    
    public MovimientosOrdena(String movimiento){
        this.movimiento = new SimpleStringProperty(movimiento);
    }
    
    //Get and Set
    public String getMovimiento(){
        return movimiento.getValue();
    }
    
    public void setMovimiento(String movimiento){
        this.movimiento = new SimpleStringProperty(movimiento);
    }
}
