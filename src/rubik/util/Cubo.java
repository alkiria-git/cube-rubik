/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rubik.util;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.paint.Color;
import rubik.model.Pieza;


/**
 *
 * @author Eduardo Salas Cerdas
 */
public class Cubo{

    /*Movimientos
    U = Up; -> U' = inversa de Up;
    D = Down; -> D' = inversa de Down;
    R = Right; -> R' = inversa de Right;
    L = Left; -> L' = inversa de Left;
    F = Front; -> F' = inversa de Front;
    B = Back; -> B' = inversa de Back;
     */
    //Atributos
    private Boolean bandera;
    private Boolean flag;
    private Integer desordena;  //Movimientos que determinan el desorden del cubo
    private Integer dimCubo;    //Dimensión del cubo
    private Pieza[][][] rubik;    // Rubik original
    private Pieza[][][] rubikAux; // Rubik temporal
    private Color frente;
    private Color derecha;
    private Color atras;
    private Color izquierda;
    private Color arriba;
    private Color abajo;
    private ArrayList<String> movimientos;
    private ArrayList<String> movimientosOrdena;
    private ObservableList<MovimientosOrdena> ordena;
    private ObservableList<MovimientosUsuarios> movimientosUsuario;

    //Constructores
    /**
     * Cubo por defecto
     */
    public Cubo() {
        inicializaComponentes();
    }

    /**
     * Cubo con colores
     *
     * @param frente
     * @param atras
     * @param arriba
     * @param abajo
     * @param derecha
     * @param izquierda
     */
    public Cubo(Color frente, Color atras, Color arriba, Color abajo, Color derecha, Color izquierda) {
        this();
        this.frente = frente;
        this.atras = atras;
        this.arriba = arriba;
        this.abajo = abajo;
        this.derecha = derecha;
        this.izquierda = izquierda;
        creaCubo();
    }

    //Métodos
    /**
     * Inicializa componentes y atributos
     */
    public void inicializaComponentes() {
        this.movimientos = new ArrayList();
        this.movimientosOrdena = new ArrayList();
        this.bandera = false;
        this.flag = false;
        this.desordena = 0;
        this.dimCubo = 3;
        this.rubik = new Pieza[this.dimCubo][this.dimCubo][this.dimCubo];
        this.rubikAux = new Pieza[this.dimCubo][this.dimCubo][this.dimCubo];
        this.ordena = FXCollections.observableArrayList();
        this.movimientosUsuario = FXCollections.observableArrayList();

        for (int z = 0; z < this.dimCubo; z++) {          //Crea las 6 caras
            for (int x = 0; x < this.dimCubo; x++) {      //Crea 3 de ancho
                for (int y = 0; y < this.dimCubo; y++) {  //Crea 3 de largo
                    this.rubik[x][y][z] = new Pieza();
                    this.rubikAux[x][y][z] = new Pieza();
                }
            }
        }

        this.frente = null;
        this.atras = null;
        this.arriba = null;
        this.abajo = null;
        this.derecha = null;
        this.izquierda = null;
    }

    /**
     * Crea las esquinas del cubo con sus respectivos colores
     */
    public void creaEsquinas() {
        //Primera Esquina
        this.rubik[0][0][0].setTipo("Esquina");
        this.rubik[0][0][0].setFrente(this.frente);
        this.rubik[0][0][0].setArriba(this.arriba);
        this.rubik[0][0][0].setIzquierda(this.izquierda);
        //Segunda Esquina
        this.rubik[0][0][2].setTipo("Esquina");
        this.rubik[0][0][2].setAtras(this.atras);
        this.rubik[0][0][2].setArriba(this.arriba);
        this.rubik[0][0][2].setIzquierda(this.izquierda);
        //Tercera Esquina
        this.rubik[0][2][0].setTipo("Esquina");
        this.rubik[0][2][0].setFrente(this.frente);
        this.rubik[0][2][0].setArriba(this.arriba);
        this.rubik[0][2][0].setDerecha(this.derecha);
        //Cuarta Esquina
        this.rubik[0][2][2].setTipo("Esquina");
        this.rubik[0][2][2].setAtras(this.atras);
        this.rubik[0][2][2].setArriba(this.arriba);
        this.rubik[0][2][2].setDerecha(this.derecha);
        //Quinta Esquina
        this.rubik[2][0][0].setTipo("Esquina");
        this.rubik[2][0][0].setFrente(this.frente);
        this.rubik[2][0][0].setAbajo(this.abajo);
        this.rubik[2][0][0].setIzquierda(this.izquierda);
        //Sexta Esquina
        this.rubik[2][0][2].setTipo("Esquina");
        this.rubik[2][0][2].setAtras(this.atras);
        this.rubik[2][0][2].setAbajo(this.abajo);
        this.rubik[2][0][2].setIzquierda(this.izquierda);
        //Setima Esquina
        this.rubik[2][2][0].setTipo("Esquina");
        this.rubik[2][2][0].setFrente(this.frente);
        this.rubik[2][2][0].setAbajo(this.abajo);
        this.rubik[2][2][0].setDerecha(this.derecha);
        //Octava Esquina
        this.rubik[2][2][2].setTipo("Esquina");
        this.rubik[2][2][2].setAtras(this.atras);
        this.rubik[2][2][2].setAbajo(this.abajo);
        this.rubik[2][2][2].setDerecha(this.derecha);
    }

    /**
     * Crea las aristas con sus respectivos colores
     */
    public void creaAristas() {
        //Frente-Arriba
        this.rubik[0][1][0].setTipo("Arista");
        this.rubik[0][1][0].setFrente(this.frente);
        this.rubik[0][1][0].setArriba(this.arriba);

        //Frente-Izquierda
        this.rubik[1][0][0].setTipo("Arista");
        this.rubik[1][0][0].setFrente(this.frente);
        this.rubik[1][0][0].setIzquierda(this.izquierda);

        //Frente-Derecha
        this.rubik[1][2][0].setTipo("Arista");
        this.rubik[1][2][0].setFrente(this.frente);
        this.rubik[1][2][0].setDerecha(this.derecha);

        //Frente-Abajo
        this.rubik[2][1][0].setTipo("Arista");
        this.rubik[2][1][0].setFrente(this.frente);
        this.rubik[2][1][0].setAbajo(this.abajo);

        //Atras-Arriba
        this.rubik[0][1][2].setTipo("Arista");
        this.rubik[0][1][2].setAtras(this.atras);
        this.rubik[0][1][2].setArriba(this.arriba);

        //Atras-Izquierda
        this.rubik[1][0][2].setTipo("Arista");
        this.rubik[1][0][2].setAtras(this.atras);
        this.rubik[1][0][2].setIzquierda(this.izquierda);

        //Atras-Derecha
        this.rubik[1][2][2].setTipo("Arista");
        this.rubik[1][2][2].setAtras(this.atras);
        this.rubik[1][2][2].setDerecha(this.derecha);

        //Atras-Abajo
        this.rubik[2][1][2].setTipo("Arista");
        this.rubik[2][1][2].setAtras(this.atras);
        this.rubik[2][1][2].setAbajo(this.abajo);

        //Izquierda-Arriba
        this.rubik[0][0][1].setTipo("Arista");
        this.rubik[0][0][1].setIzquierda(this.izquierda);
        this.rubik[0][0][1].setArriba(this.arriba);

        //Izquierda-Abajo
        this.rubik[2][0][1].setTipo("Arista");
        this.rubik[2][0][1].setIzquierda(this.izquierda);
        this.rubik[2][0][1].setAbajo(this.abajo);

        //Derecha-Arriba
        this.rubik[0][2][1].setTipo("Arista");
        this.rubik[0][2][1].setDerecha(this.derecha);
        this.rubik[0][2][1].setArriba(this.arriba);

        //Derecha-Abajo
        this.rubik[2][2][1].setTipo("Arista");
        this.rubik[2][2][1].setDerecha(this.derecha);
        this.rubik[2][2][1].setAbajo(this.abajo);
    }

    /**
     * Crea los centros con sus respectivos colores
     */
    public void creaCentros() {
        //Frente
        this.rubik[1][1][0].setTipo("Centro");
        this.rubik[1][1][0].setFrente(this.frente);
        //Derecha
        this.rubik[1][2][1].setTipo("Centro");
        this.rubik[1][2][1].setDerecha(this.derecha);
        //Atras
        this.rubik[1][1][2].setTipo("Centro");
        this.rubik[1][1][2].setAtras(this.atras);
        //Izquierda
        this.rubik[1][0][1].setTipo("Centro");
        this.rubik[1][0][1].setIzquierda(this.izquierda);
        //Arriba
        this.rubik[0][1][1].setTipo("Centro");
        this.rubik[0][1][1].setArriba(this.arriba);
        //Abajo
        this.rubik[2][1][1].setTipo("Centro");
        this.rubik[2][1][1].setAbajo(this.abajo);
    }

    /**
     * Crea el cubo
     */
    public void creaCubo() {
        creaCentros();
        creaAristas();
        creaEsquinas();
    }

    /**
     * Desordena el cubo aleatoriamente
     *
     * @param moves
     */
    public void desordenaCubo(Integer moves) {
        if (moves < 16) {
            moves = 16;
        }
        this.bandera = true;
        this.desordena = 0;
        while (bandera) {
            int[] random = Funciones.rand(0, 5);
            switch (random[0]) {
                case 0:
                    random = Funciones.rand(0, 1);
                    if (random[0] == 0) {
                        Up();
                        movimientos.add("U");
                    } else {
                        UpPrima();
                        movimientos.add("U'");
                    }
                    this.desordena++;
                    break;
                case 1:
                    random = Funciones.rand(0, 1);
                    if (random[0] == 0) {
                        Down();
                        movimientos.add("D");
                    } else {
                        DownPrima();
                        movimientos.add("D'");
                    }
                    this.desordena++;
                    break;
                case 2:
                    random = Funciones.rand(0, 1);
                    if (random[0] == 0) {
                        Rigth();
                        movimientos.add("R");
                    } else {
                        RigthPrima();
                        movimientos.add("R'");
                    }
                    this.desordena++;
                    break;
                case 3:
                    random = Funciones.rand(0, 1);
                    if (random[0] == 0) {
                        Left();
                        movimientos.add("L");
                    } else {
                        LeftPrima();
                        movimientos.add("L'");
                    }
                    this.desordena++;
                    break;
                case 4:
                    if (random[0] == 0) {
                        Front();
                        movimientos.add("F");
                    } else {
                        FrontPrima();
                        movimientos.add("F'");
                    }
                    this.desordena++;
                    break;
                case 5:
                    if (random[0] == 0) {
                        Back();
                        movimientos.add("B");
                    } else {
                        BackPrima();
                        movimientos.add("B'");
                    }
                    this.desordena++;
                    break;
                default:
                    break;
            }
            if (this.desordena > moves) {
                this.bandera = false;
            }
        }
    }

    /**
     * Iguala la matriz auxiliar con la matriz original
     */
    public void equalRubikAux() {
        for (int x = 0; x < this.dimCubo; x++) {
            for (int y = 0; y < this.dimCubo; y++) {
                for (int z = 0; z < this.dimCubo; z++) {
                    this.rubikAux[x][y][z].setTipo(this.rubik[x][y][z].getTipo());
                    this.rubikAux[x][y][z].setFrente(this.rubik[x][y][z].getFrente());
                    this.rubikAux[x][y][z].setDerecha(this.rubik[x][y][z].getDerecha());
                    this.rubikAux[x][y][z].setAtras(this.rubik[x][y][z].getAtras());
                    this.rubikAux[x][y][z].setIzquierda(this.rubik[x][y][z].getIzquierda());
                    this.rubikAux[x][y][z].setArriba(this.rubik[x][y][z].getArriba());
                    this.rubikAux[x][y][z].setAbajo(this.rubik[x][y][z].getAbajo());
                }
            }
        }
    }

    /**
     * Determina si el cubo ya está armado
     *
     * @return true si esta armado completamente
     */
    public Boolean terminado() {
        return frente() && atras() && derecha() && izquierda() && arriba() && abajo();
    }

    /**
     * Determina si el frente ya tiene todos sus colores bien ubicados
     *
     * @return true or false
     */
    public Boolean frente() {
        return this.rubik[1][1][0].getFrente().equals(this.rubik[0][0][0].getFrente())
                && this.rubik[1][1][0].getFrente().equals(this.rubik[0][1][0].getFrente())
                && this.rubik[1][1][0].getFrente().equals(this.rubik[0][2][0].getFrente())
                && this.rubik[1][1][0].getFrente().equals(this.rubik[1][0][0].getFrente())
                && this.rubik[1][1][0].getFrente().equals(this.rubik[1][2][0].getFrente())
                && this.rubik[1][1][0].getFrente().equals(this.rubik[2][0][0].getFrente())
                && this.rubik[1][1][0].getFrente().equals(this.rubik[2][1][0].getFrente())
                && this.rubik[1][1][0].getFrente().equals(this.rubik[2][2][0].getFrente());
    }

    /**
     * Determina si atras ya tiene todos sus colores bien ubicados
     *
     * @return true or false
     */
    public Boolean atras() {
        return this.rubik[0][0][2].getAtras().equals(this.rubik[0][1][2].getAtras())
                && this.rubik[0][0][2].getAtras().equals(this.rubik[0][2][2].getAtras())
                && this.rubik[0][0][2].getAtras().equals(this.rubik[1][0][2].getAtras())
                && this.rubik[0][0][2].getAtras().equals(this.rubik[1][1][2].getAtras())
                && this.rubik[0][0][2].getAtras().equals(this.rubik[1][2][2].getAtras())
                && this.rubik[0][0][2].getAtras().equals(this.rubik[2][0][2].getAtras())
                && this.rubik[0][0][2].getAtras().equals(this.rubik[2][1][2].getAtras())
                && this.rubik[0][0][2].getAtras().equals(this.rubik[2][2][2].getAtras());
    }

    /**
     * Determina si la derecha ya tiene todos colores bien ubicados
     *
     * @return true or false
     */
    public Boolean derecha() {
        return this.rubik[0][2][0].getDerecha().equals(this.rubik[0][2][1].getDerecha())
                && this.rubik[0][2][0].getDerecha().equals(this.rubik[0][2][2].getDerecha())
                && this.rubik[0][2][0].getDerecha().equals(this.rubik[1][2][0].getDerecha())
                && this.rubik[0][2][0].getDerecha().equals(this.rubik[1][2][1].getDerecha())
                && this.rubik[0][2][0].getDerecha().equals(this.rubik[1][2][2].getDerecha())
                && this.rubik[0][2][0].getDerecha().equals(this.rubik[2][2][0].getDerecha())
                && this.rubik[0][2][0].getDerecha().equals(this.rubik[2][2][1].getDerecha())
                && this.rubik[0][2][0].getDerecha().equals(this.rubik[2][2][2].getDerecha());
    }

    /**
     * Determina si la izquierda ya tiene todos sus colores bien ubicados
     *
     * @return true or false
     */
    public Boolean izquierda() {
        return this.rubik[0][0][0].getIzquierda().equals(this.rubik[0][0][1].getIzquierda())
                && this.rubik[0][0][0].getIzquierda().equals(this.rubik[0][0][2].getIzquierda())
                && this.rubik[0][0][0].getIzquierda().equals(this.rubik[1][0][0].getIzquierda())
                && this.rubik[0][0][0].getIzquierda().equals(this.rubik[1][0][1].getIzquierda())
                && this.rubik[0][0][0].getIzquierda().equals(this.rubik[1][0][2].getIzquierda())
                && this.rubik[0][0][0].getIzquierda().equals(this.rubik[2][0][0].getIzquierda())
                && this.rubik[0][0][0].getIzquierda().equals(this.rubik[2][0][1].getIzquierda())
                && this.rubik[0][0][0].getIzquierda().equals(this.rubik[2][0][2].getIzquierda());
    }

    /**
     * Determina si arriba ya tiene todos sus colores bien ubicados
     *
     * @return true or false
     */
    public Boolean arriba() {
        return this.rubik[0][0][0].getArriba().equals(this.rubik[0][1][0].getArriba())
                && this.rubik[0][0][0].getArriba().equals(this.rubik[0][2][0].getArriba())
                && this.rubik[0][0][0].getArriba().equals(this.rubik[0][0][1].getArriba())
                && this.rubik[0][0][0].getArriba().equals(this.rubik[0][1][1].getArriba())
                && this.rubik[0][0][0].getArriba().equals(this.rubik[0][2][1].getArriba())
                && this.rubik[0][0][0].getArriba().equals(this.rubik[0][0][2].getArriba())
                && this.rubik[0][0][0].getArriba().equals(this.rubik[0][1][2].getArriba())
                && this.rubik[0][0][0].getArriba().equals(this.rubik[0][2][2].getArriba());
    }

    /**
     * Determina si abajo ya tiene todos sus colores bien ubicados
     *
     * @return true or false
     */
    public Boolean abajo() {
        return this.rubik[2][0][0].getAbajo().equals(this.rubik[2][1][0].getAbajo())
                && this.rubik[2][0][0].getAbajo().equals(this.rubik[2][2][0].getAbajo())
                && this.rubik[2][0][0].getAbajo().equals(this.rubik[2][0][1].getAbajo())
                && this.rubik[2][0][0].getAbajo().equals(this.rubik[2][1][1].getAbajo())
                && this.rubik[2][0][0].getAbajo().equals(this.rubik[2][2][1].getAbajo())
                && this.rubik[2][0][0].getAbajo().equals(this.rubik[2][0][2].getAbajo())
                && this.rubik[2][0][0].getAbajo().equals(this.rubik[2][1][2].getAbajo())
                && this.rubik[2][0][0].getAbajo().equals(this.rubik[2][2][2].getAbajo());
    }

    //Básicos
    /**
     * Realiza el movimiento Up
     */
    public void Up() { //<-
        equalRubikAux();
        //Frente
        this.rubik[0][0][0].setFrente(this.rubikAux[0][2][0].getDerecha());
        this.rubik[0][1][0].setFrente(this.rubikAux[0][2][1].getDerecha());
        this.rubik[0][2][0].setFrente(this.rubikAux[0][2][2].getDerecha());
        //Derecha
        this.rubik[0][2][0].setDerecha(this.rubikAux[0][2][2].getAtras());
        this.rubik[0][2][1].setDerecha(this.rubikAux[0][1][2].getAtras());
        this.rubik[0][2][2].setDerecha(this.rubikAux[0][0][2].getAtras());
        //Atras
        this.rubik[0][2][2].setAtras(this.rubikAux[0][0][2].getIzquierda());
        this.rubik[0][1][2].setAtras(this.rubikAux[0][0][1].getIzquierda());
        this.rubik[0][0][2].setAtras(this.rubikAux[0][0][0].getIzquierda());
        //Izquierda
        this.rubik[0][0][0].setIzquierda(this.rubikAux[0][2][0].getFrente());
        this.rubik[0][0][1].setIzquierda(this.rubikAux[0][1][0].getFrente());
        this.rubik[0][0][2].setIzquierda(this.rubikAux[0][0][0].getFrente());
        //Arriba
        this.rubik[0][0][0].setArriba(this.rubikAux[0][2][0].getArriba());
        this.rubik[0][1][0].setArriba(this.rubikAux[0][2][1].getArriba());
        this.rubik[0][2][0].setArriba(this.rubikAux[0][2][2].getArriba());
        this.rubik[0][2][1].setArriba(this.rubikAux[0][1][2].getArriba());
        this.rubik[0][2][2].setArriba(this.rubikAux[0][0][2].getArriba());
        this.rubik[0][1][2].setArriba(this.rubikAux[0][0][1].getArriba());
        this.rubik[0][0][2].setArriba(this.rubikAux[0][0][0].getArriba());
        this.rubik[0][0][1].setArriba(this.rubikAux[0][1][0].getArriba());
    }

    /**
     * Realiza el movimiento Rigth
     */
    public void Rigth() {
        equalRubikAux();
        //Frente
        this.rubik[0][2][0].setFrente(this.rubikAux[2][2][0].getAbajo());
        this.rubik[1][2][0].setFrente(this.rubikAux[2][2][1].getAbajo());
        this.rubik[2][2][0].setFrente(this.rubikAux[2][2][2].getAbajo());
        //Arriba
        this.rubik[0][2][0].setArriba(this.rubikAux[2][2][0].getFrente());
        this.rubik[0][2][1].setArriba(this.rubikAux[1][2][0].getFrente());
        this.rubik[0][2][2].setArriba(this.rubikAux[0][2][0].getFrente());
        //Atras
        this.rubik[0][2][2].setAtras(this.rubikAux[0][2][0].getArriba());
        this.rubik[1][2][2].setAtras(this.rubikAux[0][2][1].getArriba());
        this.rubik[2][2][2].setAtras(this.rubikAux[0][2][2].getArriba());
        //Abajo
        this.rubik[2][2][0].setAbajo(this.rubikAux[2][2][2].getAtras());
        this.rubik[2][2][1].setAbajo(this.rubikAux[1][2][2].getAtras());
        this.rubik[2][2][2].setAbajo(this.rubikAux[0][2][2].getAtras());
        //Derecha
        this.rubik[0][2][0].setDerecha(this.rubikAux[2][2][0].getDerecha());
        this.rubik[0][2][1].setDerecha(this.rubikAux[1][2][0].getDerecha());
        this.rubik[0][2][2].setDerecha(this.rubikAux[0][2][0].getDerecha());
        this.rubik[1][2][2].setDerecha(this.rubikAux[0][2][1].getDerecha());
        this.rubik[2][2][2].setDerecha(this.rubikAux[0][2][2].getDerecha());
        this.rubik[2][2][1].setDerecha(this.rubikAux[1][2][2].getDerecha());
        this.rubik[2][2][0].setDerecha(this.rubikAux[2][2][2].getDerecha());
        this.rubik[1][2][0].setDerecha(this.rubikAux[2][2][1].getDerecha());
    }

    /**
     * Realiza el movimiento Left
     */
    public void Left() {
        equalRubikAux();
        //Frente
        this.rubik[0][0][0].setFrente(this.rubikAux[0][0][2].getArriba());
        this.rubik[1][0][0].setFrente(this.rubikAux[0][0][1].getArriba());
        this.rubik[2][0][0].setFrente(this.rubikAux[0][0][0].getArriba());
        //Abajo
        this.rubik[2][0][0].setAbajo(this.rubikAux[0][0][0].getFrente());
        this.rubik[2][0][1].setAbajo(this.rubikAux[1][0][0].getFrente());
        this.rubik[2][0][2].setAbajo(this.rubikAux[2][0][0].getFrente());
        //Atras
        this.rubik[2][0][2].setAtras(this.rubikAux[2][0][0].getAbajo());
        this.rubik[1][0][2].setAtras(this.rubikAux[2][0][1].getAbajo());
        this.rubik[0][0][2].setAtras(this.rubikAux[2][0][2].getAbajo());
        //Arriba
        this.rubik[0][0][2].setArriba(this.rubikAux[2][0][2].getAtras());
        this.rubik[0][0][1].setArriba(this.rubikAux[1][0][2].getAtras());
        this.rubik[0][0][0].setArriba(this.rubikAux[0][0][2].getAtras());
        //Izquierda
        this.rubik[0][0][0].setIzquierda(this.rubikAux[0][0][2].getIzquierda());
        this.rubik[1][0][0].setIzquierda(this.rubikAux[0][0][1].getIzquierda());
        this.rubik[2][0][0].setIzquierda(this.rubikAux[0][0][0].getIzquierda());
        this.rubik[2][0][1].setIzquierda(this.rubikAux[1][0][0].getIzquierda());
        this.rubik[2][0][2].setIzquierda(this.rubikAux[2][0][0].getIzquierda());
        this.rubik[1][0][2].setIzquierda(this.rubikAux[2][0][1].getIzquierda());
        this.rubik[0][0][2].setIzquierda(this.rubikAux[2][0][2].getIzquierda());
        this.rubik[0][0][1].setIzquierda(this.rubikAux[1][0][2].getIzquierda());
    }

    /**
     * Realiza el movimiento Down
     */
    public void Down() { //->
        equalRubikAux();
        //Frente
        this.rubik[2][0][0].setFrente(this.rubikAux[2][0][2].getIzquierda());
        this.rubik[2][1][0].setFrente(this.rubikAux[2][0][1].getIzquierda());
        this.rubik[2][2][0].setFrente(this.rubikAux[2][0][0].getIzquierda());
        //Derecha
        this.rubik[2][2][0].setDerecha(this.rubikAux[2][0][0].getFrente());
        this.rubik[2][2][1].setDerecha(this.rubikAux[2][1][0].getFrente());
        this.rubik[2][2][2].setDerecha(this.rubikAux[2][2][0].getFrente());
        //Atras
        this.rubik[2][2][2].setAtras(this.rubikAux[2][2][0].getDerecha());
        this.rubik[2][1][2].setAtras(this.rubikAux[2][2][1].getDerecha());
        this.rubik[2][0][2].setAtras(this.rubikAux[2][2][2].getDerecha());
        //Izquierda
        this.rubik[2][0][2].setIzquierda(this.rubikAux[2][2][2].getAtras());
        this.rubik[2][0][1].setIzquierda(this.rubikAux[2][1][2].getAtras());
        this.rubik[2][0][0].setIzquierda(this.rubikAux[2][0][2].getAtras());
        //Abajo
        this.rubik[2][0][0].setAbajo(this.rubikAux[2][0][2].getAbajo());
        this.rubik[2][1][0].setAbajo(this.rubikAux[2][0][1].getAbajo());
        this.rubik[2][2][0].setAbajo(this.rubikAux[2][0][0].getAbajo());
        this.rubik[2][2][1].setAbajo(this.rubikAux[2][1][0].getAbajo());
        this.rubik[2][2][2].setAbajo(this.rubikAux[2][2][0].getAbajo());
        this.rubik[2][1][2].setAbajo(this.rubikAux[2][2][1].getAbajo());
        this.rubik[2][0][2].setAbajo(this.rubikAux[2][2][2].getAbajo());
        this.rubik[2][0][1].setAbajo(this.rubikAux[2][1][2].getAbajo());
    }

    /**
     * Realiza el movimiento Front
     */
    public void Front() {
        equalRubikAux();
        ejeDerecha();
        Rigth();
        ejeIzquierda();
    }

    /**
     * Realiza el movimiento Back
     */
    public void Back() {
        equalRubikAux();
        ejeIzquierda();
        Rigth();
        ejeDerecha();
    }

    //Inversos
    /**
     * Realiza el movimiento Up Prima
     */
    public void UpPrima() {
        equalRubikAux();
        Up();
        Up();
        Up();
    }

    /**
     * Realiza el movimiento Rigth Prima
     */
    public void RigthPrima() {
        equalRubikAux();
        Rigth();
        Rigth();
        Rigth();
    }

    /**
     * Realiza el movimiento Left Prima
     */
    public void LeftPrima() {
        equalRubikAux();
        Left();
        Left();
        Left();
    }

    /**
     * Realiza el movimiento Down Prima
     */
    public void DownPrima() {
        equalRubikAux();
        Down();
        Down();
        Down();
    }

    /**
     * Realiza el movimiento Front Prima
     */
    public void FrontPrima() {
        equalRubikAux();
        Front();
        Front();
        Front();
    }

    /**
     * Realiza el movimiento Back Prima
     */
    public void BackPrima() {
        equalRubikAux();
        Back();
        Back();
        Back();
    }

    //Cambio de Ejes
    /**
     * Realiza el cambio de eje derecha
     */
    public void ejeDerecha() {
        equalRubikAux();
        UpPrima();
        //Frente
        this.rubik[1][0][0].setFrente(this.rubikAux[1][0][2].getIzquierda());
        this.rubik[1][1][0].setFrente(this.rubikAux[1][0][1].getIzquierda());
        this.rubik[1][2][0].setFrente(this.rubikAux[1][0][0].getIzquierda());
        //Derecha
        this.rubik[1][2][0].setDerecha(this.rubikAux[1][0][0].getFrente());
        this.rubik[1][2][1].setDerecha(this.rubikAux[1][1][0].getFrente());
        this.rubik[1][2][2].setDerecha(this.rubikAux[1][2][0].getFrente());
        //Atras
        this.rubik[1][2][2].setAtras(this.rubikAux[1][2][0].getDerecha());
        this.rubik[1][1][2].setAtras(this.rubikAux[1][2][1].getDerecha());
        this.rubik[1][0][2].setAtras(this.rubikAux[1][2][2].getDerecha());
        //Izquierda
        this.rubik[1][0][2].setIzquierda(this.rubikAux[1][2][2].getAtras());
        this.rubik[1][0][1].setIzquierda(this.rubikAux[1][1][2].getAtras());
        this.rubik[1][0][0].setIzquierda(this.rubikAux[1][0][2].getAtras());
        Down();
    }

    /**
     * Realiza el cambio de eje izquierda
     */
    public void ejeIzquierda() {
        equalRubikAux();
        Up();
        //Frente
        this.rubik[1][0][0].setFrente(this.rubikAux[1][2][0].getDerecha());
        this.rubik[1][1][0].setFrente(this.rubikAux[1][2][1].getDerecha());
        this.rubik[1][2][0].setFrente(this.rubikAux[1][2][2].getDerecha());
        //Derecha
        this.rubik[1][2][0].setDerecha(this.rubikAux[1][2][2].getAtras());
        this.rubik[1][2][1].setDerecha(this.rubikAux[1][1][2].getAtras());
        this.rubik[1][2][2].setDerecha(this.rubikAux[1][0][2].getAtras());
        //Atras
        this.rubik[1][2][2].setAtras(this.rubikAux[1][0][2].getIzquierda());
        this.rubik[1][1][2].setAtras(this.rubikAux[1][0][1].getIzquierda());
        this.rubik[1][0][2].setAtras(this.rubikAux[1][0][0].getIzquierda());
        //Izquierda
        this.rubik[1][0][2].setIzquierda(this.rubikAux[1][0][0].getFrente());
        this.rubik[1][0][1].setIzquierda(this.rubikAux[1][1][0].getFrente());
        this.rubik[1][0][0].setIzquierda(this.rubikAux[1][2][0].getFrente());
        DownPrima();
    }

    /**
     * Realiza el cambio de eje arriba
     */
    public void ejeArriba() {
        equalRubikAux();
        Rigth();
        //Frente
        this.rubik[0][1][0].setFrente(this.rubikAux[2][1][0].getAbajo());
        this.rubik[1][1][0].setFrente(this.rubikAux[2][1][1].getAbajo());
        this.rubik[2][1][0].setFrente(this.rubikAux[2][1][2].getAbajo());
        //Arriba
        this.rubik[0][1][0].setArriba(this.rubikAux[2][1][0].getFrente());
        this.rubik[0][1][1].setArriba(this.rubikAux[1][1][0].getFrente());
        this.rubik[0][1][2].setArriba(this.rubikAux[0][1][0].getFrente());
        //Atras
        this.rubik[0][1][2].setAtras(this.rubikAux[0][1][0].getArriba());
        this.rubik[1][1][2].setAtras(this.rubikAux[0][1][1].getArriba());
        this.rubik[2][1][2].setAtras(this.rubikAux[0][1][2].getArriba());
        //Abajo
        this.rubik[2][1][0].setAbajo(this.rubikAux[2][1][2].getAtras());
        this.rubik[2][1][1].setAbajo(this.rubikAux[1][1][2].getAtras());
        this.rubik[2][1][2].setAbajo(this.rubikAux[0][1][2].getAtras());
        LeftPrima();
    }

    /**
     * Realiza el cambio de eje abajo
     */
    public void ejeAbajo() {
        equalRubikAux();
        RigthPrima();
        //Frente
        this.rubik[0][1][0].setFrente(this.rubikAux[0][1][2].getArriba());
        this.rubik[1][1][0].setFrente(this.rubikAux[0][1][1].getArriba());
        this.rubik[2][1][0].setFrente(this.rubikAux[0][1][0].getArriba());
        //Arriba
        this.rubik[0][1][0].setArriba(this.rubikAux[0][1][2].getAtras());
        this.rubik[0][1][1].setArriba(this.rubikAux[1][1][2].getAtras());
        this.rubik[0][1][2].setArriba(this.rubikAux[2][1][2].getAtras());
        //Atras
        this.rubik[0][1][2].setAtras(this.rubikAux[2][1][2].getAbajo());
        this.rubik[1][1][2].setAtras(this.rubikAux[2][1][1].getAbajo());
        this.rubik[2][1][2].setAtras(this.rubikAux[2][1][0].getAbajo());
        //Abajo
        this.rubik[2][1][0].setAbajo(this.rubikAux[0][1][0].getFrente());
        this.rubik[2][1][1].setAbajo(this.rubikAux[1][1][0].getFrente());
        this.rubik[2][1][2].setAbajo(this.rubikAux[2][1][0].getFrente());
        Left();
    }

    /**
     * Recibe un color y lo color y lo convierte a String luego aplica el style
     * al label
     *
     * @param color
     * @return String
     */
    public String Style(String color) {
        return "-fx-background-color: "+ color+"\n-fx-border-color: #CFB53B;";
    }

    /**
     * Recibe un color y lo convierte a hexadecimal
     *
     * @param color
     * @return String
     */
    public String colorHex(Color color) {
        return "#" + Integer.toHexString(color.hashCode()).substring(0, 6).toUpperCase() + ";";
    }

    public Color getFrente() {
        return frente;
    }

    public void setFrente(Color frente) {
        this.frente = frente;
    }

    public Color getDerecha() {
        return derecha;
    }

    public void setDerecha(Color derecha) {
        this.derecha = derecha;
    }

    public Color getAtras() {
        return atras;
    }

    public void setAtras(Color atras) {
        this.atras = atras;
    }

    public Color getIzquierda() {
        return izquierda;
    }

    public void setIzquierda(Color izquierda) {
        this.izquierda = izquierda;
    }

    public Color getArriba() {
        return arriba;
    }

    public void setArriba(Color arriba) {
        this.arriba = arriba;
    }

    public Color getAbajo() {
        return abajo;
    }

    public void setAbajo(Color abajo) {
        this.abajo = abajo;
    }

    public Pieza[][][] getRubik() {
        return rubik;
    }

    public ArrayList<String> getMovimientos() {
        return movimientos;
    }

    /**
     * Resuelve el cubo
     */
    public void algoritmoSolucion() {
        if (!terminado()) {
            //Primera Capa
            cruzAbajo();
            esquinasAbajo();

            //Segunda Capa
            aristasCentrales();

            //Tercera Capa
            cruzArriba();
            ordenaAristas();
            ordenaEsquinas();
            acomodaEsquinas();
            ordenaCubo();

            if (!terminado() && !flag) {
                flag = true;
                algoritmoSolucion();
            }
            if (terminado()) {
                this.movimientos.clear();
            }
        } else {
            this.movimientos.clear();
        }
    }

    //Primera Capa
    /**
     * Realiza una serie de movimientos para resolver la cruz de abajo
     */
    public void cruzAbajo() {
        Integer termina = 0;
        //Frente
        Color down = rubik[2][1][1].getAbajo();
        while (termina < 50) {
            if (rubik[1][0][0].getFrente().equals(down)) { //Arista en lado izquierdo
                LeftPrima();
                UpPrima();
                Left();
                this.movimientosOrdena.add("L'");
                this.movimientosOrdena.add("U'");
                this.movimientosOrdena.add("L");
                aristasAbajo();
            } else if (rubik[0][1][0].getFrente().equals(down)) { //Arista Arriba
                FrontPrima();
                LeftPrima();
                Up();
                Left();
                UpPrima();
                Front();
                UpPrima();
                this.movimientosOrdena.add("F'");
                this.movimientosOrdena.add("L'");
                this.movimientosOrdena.add("U");
                this.movimientosOrdena.add("L");
                this.movimientosOrdena.add("U'");
                this.movimientosOrdena.add("F");
                this.movimientosOrdena.add("U'");
                aristasAbajo();
            } else if (rubik[1][2][0].getFrente().equals(down)) { //Arista  derecha
                Rigth();
                Up();
                RigthPrima();
                this.movimientosOrdena.add("R");
                this.movimientosOrdena.add("U");
                this.movimientosOrdena.add("R'");
                aristasAbajo();
            } else if (rubik[2][1][0].getFrente().equals(down)) { // Arista abajo
                Front();
                LeftPrima();
                UpPrima();
                Left();
                this.movimientosOrdena.add("F");
                this.movimientosOrdena.add("L'");
                this.movimientosOrdena.add("U'");
                this.movimientosOrdena.add("L");
                aristasAbajo();
            } else if (rubik[0][1][0].getArriba().equals(down)) {
                aristasAbajo();
            } else if (rubik[2][1][0].getAbajo().equals(down) && !rubik[2][1][0].getFrente().equals(rubik[1][1][0].getFrente())) {
                Front();
                LeftPrima();
                UpPrima();
                Left();
                FrontPrima();
                LeftPrima();
                Up();
                Left();
                UpPrima();
                Front();
                UpPrima();
                this.movimientosOrdena.add("F");
                this.movimientosOrdena.add("L'");
                this.movimientosOrdena.add("U'");
                this.movimientosOrdena.add("L");
                this.movimientosOrdena.add("F'");
                this.movimientosOrdena.add("L'");
                this.movimientosOrdena.add("U");
                this.movimientosOrdena.add("L");
                this.movimientosOrdena.add("U'");
                this.movimientosOrdena.add("F");
                this.movimientosOrdena.add("U'");
                aristasAbajo();
            }
            if (this.rubik[2][1][0].getAbajo().equals(down) && this.rubik[2][1][0].getFrente().equals(this.rubik[1][1][0].getFrente())
                    && this.rubik[2][0][1].getAbajo().equals(down) && this.rubik[2][0][1].getIzquierda().equals(this.rubik[1][0][1].getIzquierda())
                    && this.rubik[2][1][2].getAbajo().equals(down) && this.rubik[2][1][2].getAtras().equals(this.rubik[1][1][2].getAtras())
                    && this.rubik[2][2][1].getAbajo().equals(down) && this.rubik[2][2][1].getDerecha().equals(this.rubik[1][2][1].getDerecha())) {
                termina = 50;
            } else {
                ejeDerecha();
                this.movimientosOrdena.add("Eje Derecha");
            }
            termina++;
        }
    }

    /**
     * Realiza una serie de movimientos para resolver las aristas de abajo
     */
    public void aristasAbajo() {
        if (rubik[0][1][0].getFrente().equals(rubik[1][0][1].getIzquierda())) { //Izquierda
            Up();
            Left();
            Left();
            this.movimientosOrdena.add("U");
            this.movimientosOrdena.add("L");
            this.movimientosOrdena.add("L");
        } else if (rubik[0][1][0].getFrente().equals(rubik[1][2][1].getDerecha())) { //Derecha
            UpPrima();
            Rigth();
            Rigth();
            this.movimientosOrdena.add("U'");
            this.movimientosOrdena.add("R");
            this.movimientosOrdena.add("R");
        } else if (rubik[0][1][0].getFrente().equals(rubik[1][1][2].getAtras())) { //Atras
            Up();
            Up();
            Back();
            Back();
            this.movimientosOrdena.add("U");
            this.movimientosOrdena.add("U");
            this.movimientosOrdena.add("B");
            this.movimientosOrdena.add("B");
        } else if (rubik[0][1][0].getFrente().equals(rubik[1][1][0].getFrente())) { //Frente
            Front();
            Front();
            this.movimientosOrdena.add("F");
            this.movimientosOrdena.add("F");
        }
    }

    /**
     * Realiza una serie de movimientos para resolver las esquinas de abajo
     */
    public void esquinasAbajo() {
        Color down = rubik[2][1][1].getAbajo();
        Integer termina = 0;
        while (termina < 50) {
            if (rubik[0][0][0].getFrente().equals(down) || rubik[0][0][0].getArriba().equals(down) || rubik[0][0][0].getIzquierda().equals(down)) {
                if (rubik[0][0][0].getFrente().equals(down)) {
                    if (rubik[0][0][0].getArriba().equals(rubik[1][2][1].getDerecha()) && rubik[0][0][0].getIzquierda().equals(rubik[1][1][0].getFrente())) {
                        UpPrima();
                        Rigth();
                        Up();
                        RigthPrima();
                        UpPrima();
                        this.movimientosOrdena.add("U'");
                        this.movimientosOrdena.add("R");
                        this.movimientosOrdena.add("U");
                        this.movimientosOrdena.add("R'");
                        this.movimientosOrdena.add("U'");
                    } else if (rubik[0][0][0].getArriba().equals(rubik[1][0][1].getIzquierda()) && rubik[0][0][0].getIzquierda().equals(rubik[1][1][2].getAtras())) {
                        Up();
                        ejeDerecha();
                        ejeDerecha();
                        Rigth();
                        Up();
                        RigthPrima();
                        UpPrima();
                        this.movimientosOrdena.add("U");
                        this.movimientosOrdena.add("eD");
                        this.movimientosOrdena.add("eD");
                        this.movimientosOrdena.add("R");
                        this.movimientosOrdena.add("U");
                        this.movimientosOrdena.add("R'");
                        this.movimientosOrdena.add("U'");
                    } else if (rubik[0][0][0].getArriba().equals(rubik[1][1][2].getAtras()) && rubik[0][0][0].getIzquierda().equals(rubik[1][2][1].getDerecha())) {
                        UpPrima();
                        UpPrima();
                        ejeIzquierda();
                        Rigth();
                        Up();
                        RigthPrima();
                        UpPrima();
                        this.movimientosOrdena.add("U'");
                        this.movimientosOrdena.add("U'");
                        this.movimientosOrdena.add("eI");
                        this.movimientosOrdena.add("R");
                        this.movimientosOrdena.add("U");
                        this.movimientosOrdena.add("R'");
                        this.movimientosOrdena.add("U'");
                    } else if (rubik[0][0][0].getArriba().equals(rubik[1][1][0].getFrente()) && rubik[0][0][0].getIzquierda().equals(rubik[1][0][1].getIzquierda())) {
                        ejeDerecha();
                        Rigth();
                        Up();
                        RigthPrima();
                        UpPrima();
                        ejeIzquierda();
                        this.movimientosOrdena.add("eD");
                        this.movimientosOrdena.add("R");
                        this.movimientosOrdena.add("U");
                        this.movimientosOrdena.add("R'");
                        this.movimientosOrdena.add("U'");
                        this.movimientosOrdena.add("eI");
                    }
                } else if (rubik[0][0][0].getIzquierda().equals(down)) {
                    if (rubik[0][0][0].getFrente().equals(rubik[1][1][0].getFrente()) && rubik[0][0][0].getArriba().equals(rubik[1][0][1].getIzquierda())) {
                        LeftPrima();
                        UpPrima();
                        Left();
                        Up();
                        this.movimientosOrdena.add("L'");
                        this.movimientosOrdena.add("U'");
                        this.movimientosOrdena.add("L");
                        this.movimientosOrdena.add("U");
                    } else {
                        Up();
                        this.movimientosOrdena.add("U");
                    }
                } else if (rubik[0][0][0].getArriba().equals(down)) {
                    Up();
                    Rigth();
                    UpPrima();
                    UpPrima();
                    UpPrima();
                    RigthPrima();
                    Up();
                    ejeIzquierda();
                    this.movimientosOrdena.add("U");
                    this.movimientosOrdena.add("R");
                    this.movimientosOrdena.add("U'");
                    this.movimientosOrdena.add("U'");
                    this.movimientosOrdena.add("U'");
                    this.movimientosOrdena.add("R'");
                    this.movimientosOrdena.add("U");
                    this.movimientosOrdena.add("eI");
                }
            } else if (rubik[2][0][0].getFrente().equals(down) || rubik[2][0][0].getIzquierda().equals(down)) {
                LeftPrima();
                UpPrima();
                Left();
                Up();
                this.movimientosOrdena.add("L'");
                this.movimientosOrdena.add("U'");
                this.movimientosOrdena.add("L");
                this.movimientosOrdena.add("U");
            } else if (rubik[2][2][0].getFrente().equals(down) || rubik[2][2][0].getDerecha().equals(down)) {
                Rigth();
                UpPrima();
                RigthPrima();
                Up();
                this.movimientosOrdena.add("R");
                this.movimientosOrdena.add("U'");
                this.movimientosOrdena.add("R'");
                this.movimientosOrdena.add("U");
            }//Evalua las esquinas de abajo
            else if (rubik[2][0][0].getAbajo().equals(down) && (!rubik[2][0][0].getFrente().equals(rubik[1][1][0].getFrente()) || !rubik[2][0][0].getIzquierda().equals(rubik[1][0][1].getIzquierda()))) {
                LeftPrima();
                UpPrima();
                Left();
                Up();
                ejeIzquierda();
                this.movimientosOrdena.add("L'");
                this.movimientosOrdena.add("U'");
                this.movimientosOrdena.add("L");
                this.movimientosOrdena.add("U");
                this.movimientosOrdena.add("eI");
            } else if (rubik[2][2][0].getAbajo().equals(down) && (!rubik[2][2][0].getFrente().equals(rubik[1][1][0].getFrente()) || !rubik[2][2][0].getDerecha().equals(rubik[1][2][1].getDerecha()))) {
                Rigth();
                Up();
                RigthPrima();
                UpPrima();
                ejeIzquierda();
                this.movimientosOrdena.add("R");
                this.movimientosOrdena.add("U");
                this.movimientosOrdena.add("R'");
                this.movimientosOrdena.add("U'");
                this.movimientosOrdena.add("eI");
            }
            ejeDerecha();
            this.movimientosOrdena.add("eD");
            if (evaluaEsquinasPBase()) {
                termina = 50;
            }
            termina++;
        }
    }

    /**
     * Evalua si las esquinas de la primera base están bien ubicadas
     * @return true or false
     */
    public Boolean evaluaEsquinasPBase() {
        Color down = rubik[2][1][1].getAbajo();
        return rubik[2][0][0].getAbajo().equals(down) && rubik[2][0][0].getFrente().equals(rubik[1][1][0].getFrente()) && rubik[2][0][0].getIzquierda().equals(rubik[1][0][1].getIzquierda())
                && rubik[2][0][2].getAbajo().equals(down) && rubik[2][0][2].getAtras().equals(rubik[1][1][2].getAtras()) && rubik[2][0][2].getIzquierda().equals(rubik[1][0][1].getIzquierda())
                && rubik[2][2][0].getAbajo().equals(down) && rubik[2][2][0].getFrente().equals(rubik[1][1][0].getFrente()) && rubik[2][2][0].getDerecha().equals(rubik[1][2][1].getDerecha())
                && rubik[2][2][2].getAbajo().equals(down) && rubik[2][2][2].getDerecha().equals(rubik[1][2][1].getDerecha()) && rubik[2][2][2].getAtras().equals(rubik[1][1][2].getAtras());
    }

    //Segunda Capa
    /**
     * Realiza una serie de movimientos para resolver las aristas centrales
     */
    public void aristasCentrales() {
        Color up = rubik[0][1][1].getArriba();
        Integer termina = 0;
        if (!evaluaSegundaCapa()) {
            while (termina < 50) {
                if (!rubik[0][1][0].getFrente().equals(up) && !rubik[0][1][0].getArriba().equals(up)) {
                    if (rubik[0][1][0].getFrente().equals(rubik[1][1][0].getFrente())) {
                        acomodaPieza();
                    } else if (rubik[0][1][0].getFrente().equals(rubik[1][2][1].getDerecha())) {
                        UpPrima();
                        ejeIzquierda();
                        this.movimientosOrdena.add("U'");
                        this.movimientosOrdena.add("eI");
                        acomodaPieza();
                    } else if (rubik[0][1][0].getFrente().equals(rubik[1][1][2].getAtras())) {
                        Up();
                        Up();
                        ejeDerecha();
                        ejeDerecha();
                        this.movimientosOrdena.add("U");
                        this.movimientosOrdena.add("U");
                        this.movimientosOrdena.add("eD");
                        this.movimientosOrdena.add("eD");
                        acomodaPieza();
                    } else if (rubik[0][1][0].getFrente().equals(rubik[1][0][1].getIzquierda())) {
                        Up();
                        ejeDerecha();
                        this.movimientosOrdena.add("U");
                        this.movimientosOrdena.add("eD");
                        acomodaPieza();
                    }
                } else if (!rubik[1][0][0].getFrente().equals(up) && !rubik[1][0][0].getIzquierda().equals(up)
                        && !rubik[1][0][0].getFrente().equals(rubik[1][1][0].getFrente()) && !rubik[1][0][0].getIzquierda().equals(rubik[1][0][1].getIzquierda())) {
                    Up();
                    this.movimientosOrdena.add("U");
                    aristaIzquierda();
                } else if (!rubik[1][2][0].getFrente().equals(up) && !rubik[1][2][0].getDerecha().equals(up)
                        && !rubik[1][2][0].getFrente().equals(rubik[1][1][0].getFrente()) && !rubik[1][2][0].getDerecha().equals(rubik[1][2][1].getDerecha())) {
                    UpPrima();
                    this.movimientosOrdena.add("U'");
                    aristaDerecha();
                } else {
                    ejeDerecha();
                    this.movimientosOrdena.add("eD");
                }
                if (termina == 35) {
                    Front();
                    Rigth();
                    Up();
                    RigthPrima();
                    UpPrima();
                    FrontPrima();
                    this.movimientosOrdena.add("F");
                    this.movimientosOrdena.add("R");
                    this.movimientosOrdena.add("U");
                    this.movimientosOrdena.add("R'");
                    this.movimientosOrdena.add("U'");
                    this.movimientosOrdena.add("F'");
                }
                termina++;
                if (evaluaSegundaCapa()) {
                    termina = 50;
                }
            }
        }
    }

    /**
     * Realiza una serie de movimientos para acomodar las aristas en sus sitios correspondientes
     */
    public void acomodaPieza() {
        if (rubik[0][1][0].getArriba().equals(rubik[1][2][1].getDerecha())) {
            aristaDerecha();
        } else if (rubik[0][1][0].getArriba().equals(rubik[1][0][1].getIzquierda())) {
            aristaIzquierda();
        }
    }

    /**
     * Realiza una serie de movimientos para acomodar una arista en la derecha
     */
    public void aristaDerecha() {
        Up();
        Rigth();
        Up();
        RigthPrima();
        UpPrima();
        ejeIzquierda();
        LeftPrima();
        UpPrima();
        Left();
        Up();
        this.movimientosOrdena.add("U");
        this.movimientosOrdena.add("R");
        this.movimientosOrdena.add("U");
        this.movimientosOrdena.add("R'");
        this.movimientosOrdena.add("U'");
        this.movimientosOrdena.add("eI");
        this.movimientosOrdena.add("L'");
        this.movimientosOrdena.add("U'");
        this.movimientosOrdena.add("L");
        this.movimientosOrdena.add("U");
    }

    /**
     * Realiza una serie de movimientos para acomodar una arista en la izquierda
     */
    public void aristaIzquierda() {
        UpPrima();
        LeftPrima();
        UpPrima();
        Left();
        Up();
        ejeDerecha();
        Rigth();
        Up();
        RigthPrima();
        UpPrima();
        this.movimientosOrdena.add("U'");
        this.movimientosOrdena.add("L'");
        this.movimientosOrdena.add("U'");
        this.movimientosOrdena.add("L");
        this.movimientosOrdena.add("U");
        this.movimientosOrdena.add("eD");
        this.movimientosOrdena.add("R");
        this.movimientosOrdena.add("U");
        this.movimientosOrdena.add("R'");
        this.movimientosOrdena.add("U'");
    }

    /**
     * Evalua si la segunda capa está bien ubicada
     * @return true or false
     */
    public Boolean evaluaSegundaCapa() {
        return rubik[1][0][0].getFrente().equals(rubik[1][1][0].getFrente()) && rubik[1][0][0].getIzquierda().equals(rubik[1][0][1].getIzquierda())
                && rubik[1][2][0].getFrente().equals(rubik[1][1][0].getFrente()) && rubik[1][2][0].getDerecha().equals(rubik[1][2][1].getDerecha())
                && rubik[1][2][2].getDerecha().equals(rubik[1][2][1].getDerecha()) && rubik[1][2][2].getAtras().equals(rubik[1][1][2].getAtras())
                && rubik[1][0][2].getAtras().equals(rubik[1][1][2].getAtras()) && rubik[1][0][2].getIzquierda().equals(rubik[1][0][1].getIzquierda());
    }

    //Tercera Capa
    /**
     * Realiza una serie de movimientos para resolver la cruz de arriba
     */
    public void cruzArriba() {
        Color up = rubik[0][1][1].getArriba();
        Integer termina = 0;
        while (termina < 50) {
            if (raya()) {
                comandoCreaCruzUp(up);
            } else if (miniEle()) {
                Up();
                Up();
                this.movimientosOrdena.add("U");
                this.movimientosOrdena.add("U");
                comandoCreaCruzUp(up);
            } else if (punto()) {
                comandoCreaCruzUp(up);
            } else {
                Up();
                this.movimientosOrdena.add("U");
            }
            if (cruzUp()) {
                termina = 50;
            }
            termina++;
        }
    }

    /**
     * Realiza una serie de movimientos para crear la cruz de arriba
     * @param up 
     */
    public void comandoCreaCruzUp(Color up) {
        if (rubik[0][1][0].getFrente().equals(up)) {
            Front();
            Rigth();
            Up();
            RigthPrima();
            UpPrima();
            FrontPrima();
            this.movimientosOrdena.add("F");
            this.movimientosOrdena.add("R");
            this.movimientosOrdena.add("U");
            this.movimientosOrdena.add("R'");
            this.movimientosOrdena.add("U'");
            this.movimientosOrdena.add("F'");
        } else {
            Up();
            this.movimientosOrdena.add("U");
        }
    }

    /**
     * Evalua si arriba solo se encuentra la pieza central correspondiente al color
     * @return true or false
     */
    public Boolean punto() {
        Color up = rubik[0][1][1].getArriba();
        return !rubik[0][0][1].getArriba().equals(up) && !rubik[0][1][0].getArriba().equals(up) && !rubik[0][2][1].getArriba().equals(up) && !rubik[0][1][2].getArriba().equals(up);
    }

    /**
     * Evalua si arriba hay 2 piezas formando una mini "L" del mismo color a la pieza central
     * @return true or false
     */
    public Boolean miniEle() {
        Color up = rubik[0][1][1].getArriba();
        return rubik[0][1][0].getArriba().equals(up) && rubik[0][2][1].getArriba().equals(up);
    }

    /**
     * Evalua si arriba hay 2 piezas formando una linea con la pieza central
     * @return true or false
     */
    public Boolean raya() {
        Color up = rubik[0][1][1].getArriba();
        return rubik[0][0][1].getArriba().equals(up) && rubik[0][2][1].getArriba().equals(up);
    }

    /**
     * Evalua si la cruz de arriba existe
     * @return true or false
     */
    public Boolean cruzUp() {
        Color up = rubik[0][1][1].getArriba();
        return rubik[0][0][1].getArriba().equals(up) && rubik[0][2][1].getArriba().equals(up) && rubik[0][1][0].getArriba().equals(up) && rubik[0][1][2].getArriba().equals(up);
    }

    /**
     * Realiza una serie de movimientos para ordenar las aristas en sus respectivos sitios
     */
    public void ordenaAristas() {
        Integer aristas = 0, termina = 0;
        while (termina < 50) {
            if (rubik[0][1][0].getFrente().equals(rubik[1][1][0].getFrente())) {
                aristas++;
            }
            if (rubik[0][2][1].getDerecha().equals(rubik[1][2][1].getDerecha())) {
                aristas++;
            }
            if (rubik[0][0][1].getIzquierda().equals(rubik[1][0][1].getIzquierda())) {
                aristas++;
            }
            if (rubik[0][1][2].getAtras().equals(rubik[1][1][2].getAtras())) {
                aristas++;
            }

            if (aristas == 0 || aristas == 1) {
                if (rubik[0][1][0].getFrente().equals(rubik[1][1][0].getFrente())) {
                } else if (rubik[0][2][1].getDerecha().equals(rubik[1][2][1].getDerecha())) {
                    ejeIzquierda();
                    this.movimientosOrdena.add("eI");
                } else if (rubik[0][0][1].getIzquierda().equals(rubik[1][0][1].getIzquierda())) {
                    ejeDerecha();
                    this.movimientosOrdena.add("eD");
                } else if (rubik[0][1][2].getAtras().equals(rubik[1][1][2].getAtras())) {
                    ejeDerecha();
                    ejeDerecha();
                    this.movimientosOrdena.add("eD");
                    this.movimientosOrdena.add("eD");
                }
                Rigth();
                Up();
                RigthPrima();
                Up();
                Rigth();
                Up();
                Up();
                RigthPrima();
                this.movimientosOrdena.add("R");
                this.movimientosOrdena.add("U");
                this.movimientosOrdena.add("R'");
                this.movimientosOrdena.add("U");
                this.movimientosOrdena.add("R");
                this.movimientosOrdena.add("U");
                this.movimientosOrdena.add("U");
                this.movimientosOrdena.add("R'");
            } else if (aristas == 4) {
                termina = 50;
            } else {
                Up();
                this.movimientosOrdena.add("U");
            }
            aristas = 0;
            termina++;
        }
    }

    /**
     * Realiza una serie de movimientos para ordenar las esquinas en sus respectivos sitios
     */
    public void ordenaEsquinas() {
        Integer termina = 0;
        if (evaluaEsquinas() < 4) {
            while (termina < 50) {
                if (evaluaEsquinas() == 1) {
                    movimientosOrdenaEsquinas();
                }
                Rigth();
                Up();
                RigthPrima();
                UpPrima();
                LeftPrima();
                Up();
                Rigth();
                UpPrima();
                Left();
                RigthPrima();
                this.movimientosOrdena.add("R");
                this.movimientosOrdena.add("U");
                this.movimientosOrdena.add("R'");
                this.movimientosOrdena.add("U'");
                this.movimientosOrdena.add("L'");
                this.movimientosOrdena.add("U");
                this.movimientosOrdena.add("R");
                this.movimientosOrdena.add("U'");
                this.movimientosOrdena.add("L");
                this.movimientosOrdena.add("R'");
                if (evaluaEsquinas() == 4) {
                    termina = 50;
                }
                termina++;
            }
        }
    }

    /**
     * Evalua si las esquinas se encuentran bien ubicadas
     * @return cantidad de esquinas bien ubicadas
     */
    public Integer evaluaEsquinas() {
        Integer termina = 0, cantEsquinas = 0;
        Color front, left, up;
        while (termina < 4) {
            front = rubik[0][0][0].getFrente();
            left = rubik[0][0][0].getIzquierda();
            up = rubik[0][0][0].getArriba();
            if ((front.equals(rubik[1][1][0].getFrente()) || front.equals(rubik[1][0][1].getIzquierda()) || front.equals(rubik[0][1][1].getArriba()))
                    && (left.equals(rubik[1][1][0].getFrente()) || left.equals(rubik[1][0][1].getIzquierda()) || left.equals(rubik[0][1][1].getArriba()))
                    && (up.equals(rubik[1][1][0].getFrente()) || up.equals(rubik[1][0][1].getIzquierda()) || up.equals(rubik[0][1][1].getArriba()))) {
                cantEsquinas++;
            }
            ejeDerecha();
            this.movimientosOrdena.add("eD");
            if (cantEsquinas == 4) {
                termina = 4;
            }
            termina++;
        }
        return cantEsquinas;
    }

    /**
     * Realiza una serie de movimientos para ordenar las esquinas
     */
    public void movimientosOrdenaEsquinas() {
        Integer termina = 0;
        Color front, left, up;
        while (termina < 4) {
            front = rubik[0][0][0].getFrente();
            left = rubik[0][0][0].getIzquierda();
            up = rubik[0][0][0].getArriba();
            if ((front.equals(rubik[1][1][0].getFrente()) || front.equals(rubik[1][0][1].getIzquierda()) || front.equals(rubik[0][1][1].getArriba()))
                    && (left.equals(rubik[1][1][0].getFrente()) || left.equals(rubik[1][0][1].getIzquierda()) || left.equals(rubik[0][1][1].getArriba()))
                    && (up.equals(rubik[1][1][0].getFrente()) || up.equals(rubik[1][0][1].getIzquierda()) || up.equals(rubik[0][1][1].getArriba()))) {
                termina = 4;
                ejeDerecha();
                this.movimientosOrdena.add("eD");
            } else {
                ejeDerecha();
                this.movimientosOrdena.add("eD");
            }
            termina++;
        }
    }

    /**
     * Realiza una serie de movimientos para acomodar las esquinas con sus respectivos colores
     */
    public void acomodaEsquinas() {
        Integer termina = 0;
        if (!esquinasAcomodadas()) {
            while (termina < 50) {
                if (esquinasAcomodadas()) {
                    termina = 50;
                }
                if (rubik[0][2][0].getFrente().equals(rubik[0][1][0].getFrente()) && rubik[0][2][0].getDerecha().equals(rubik[0][2][1].getDerecha())) {
                    UpPrima();
                    this.movimientosOrdena.add("U'");
                } else {
                    RigthPrima();
                    DownPrima();
                    Rigth();
                    Down();
                    this.movimientosOrdena.add("R'");
                    this.movimientosOrdena.add("D'");
                    this.movimientosOrdena.add("R");
                    this.movimientosOrdena.add("D");
                }
                termina++;
            }
        }
    }

    /**
     * Evalua si las esquinas se encuentran bien ubicadas
     * @return true or false
     */
    public Boolean esquinasAcomodadas() {
        Integer termina = 0;
        if (rubik[0][2][0].getFrente().equals(rubik[1][1][0].getFrente()) && rubik[0][2][0].getDerecha().equals(rubik[1][2][1].getDerecha())) {
            termina++;
        }
        if (rubik[0][0][0].getFrente().equals(rubik[1][1][0].getFrente()) && rubik[0][0][0].getIzquierda().equals(rubik[1][0][1].getIzquierda())) {
            termina++;
        }
        if (rubik[0][0][2].getAtras().equals(rubik[1][1][2].getAtras()) && rubik[0][0][2].getIzquierda().equals(rubik[1][0][1].getIzquierda())) {
            termina++;
        }
        if (rubik[0][2][2].getAtras().equals(rubik[1][1][2].getAtras()) && rubik[0][2][2].getDerecha().equals(rubik[1][2][1].getDerecha())) {
            termina++;
        }
        return termina == 4;
    }

    /**
     * Ordena totalmente el cubo
     */
    public void ordenaCubo() {
        Integer termina = 0;
        while (termina < 50) {
            if (rubik[0][1][0].getFrente().equals(rubik[1][1][0].getFrente())) {
                termina = 50;
            } else {
                Up();
                this.movimientosOrdena.add("U");
            }
            termina++;
        }
    }

    /**
     * Copia los movimientos para ordenar el cubo para mostrarlos luego en la tabla de solucion
     */
    public void copyArrayListToObservableList() {
        for (int i = 0; i < this.movimientosOrdena.size(); i++) {
            this.ordena.add(new MovimientosOrdena(this.movimientosOrdena.get(i)));
        }
    }

    public ObservableList<MovimientosUsuarios> getMovimientosUsuario() {
        return movimientosUsuario;
    }

    public ArrayList<String> getMovimientosOrdena() {
        return movimientosOrdena;
    }

    public void setFlag(Boolean flag) {
        this.flag = flag;
    }

    public ObservableList<MovimientosOrdena> getOrdena() {
        return ordena;
    }
    
}
