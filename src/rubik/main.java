/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rubik;

import javafx.application.Application;
import javafx.stage.Stage;
import rubik.util.FlowController;

/**
 *
 * @author Eduardo Salas Cerdas
 */
public class main extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        FlowController.getInstance().InitializeFlow(primaryStage, null);
        FlowController.getInstance().goViewInWindow("Saludo");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
