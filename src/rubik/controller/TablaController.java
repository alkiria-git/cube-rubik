/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rubik.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import rubik.util.TablaPosiciones;
import rubik.util.AppContext;

/**
 * FXML Controller class
 *
 * @author
 */
public class TablaController extends Controller implements Initializable {

    @FXML
    private HBox contentPosiciones;
    @FXML
    private TableView<TablaPosiciones> tablaPosiciones;
    @FXML
    private TableColumn<TablaPosiciones, String> colNombre;
    @FXML
    private TableColumn<TablaPosiciones, Integer> colPuntaje;
    @FXML
    private TableColumn<TablaPosiciones, String> colMovimientos;
    @FXML
    private TableColumn<TablaPosiciones, String> colTiempo;

 

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        inicializa();
    }

    @Override
    public void initialize() {
    }

    

    public void inicializa() {
        this.colNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        this.colPuntaje.setCellValueFactory(new PropertyValueFactory<>("puntaje"));
        this.colMovimientos.setCellValueFactory(new PropertyValueFactory<>("movimientos"));
        this.colTiempo.setCellValueFactory(new PropertyValueFactory<>("tiempo"));
        this.tablaPosiciones.setItems(FXCollections.observableArrayList(((TablaPosiciones) AppContext.getInstance().get("tp")).getTabla()));
    }

    

}
