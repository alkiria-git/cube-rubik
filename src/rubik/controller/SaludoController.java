/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rubik.controller;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import rubik.util.FlowController;
import rubik.util.TablaPosiciones;
import rubik.util.AppContext;

/**
 * FXML Controller class
 *
 * @author
 */
public class SaludoController extends Controller implements Initializable {

    @FXML
    private AnchorPane apSaludo;
    
    TablaPosiciones tp;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        tiempo(1);

    }

    @Override
    public void initialize() {

    }

    public void tiempo(Integer tiempo) {
        this.tp = new TablaPosiciones();
        AppContext.getInstance().set("tp", this.tp);
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            int second = 1;

            @Override
            public void run() {
                Platform.runLater(() -> {
                    second--;
                    if (second < 0) {
                        timer.cancel();
                        FlowController.getInstance().goMain();
                        ((Stage) apSaludo.getScene().getWindow()).close();
                    }
                });
            }
        };

        timer.schedule(task,
                0, 800);
    }

}
