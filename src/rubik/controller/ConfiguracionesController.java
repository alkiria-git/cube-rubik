/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rubik.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXColorPicker;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXToggleButton;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import rubik.util.AppContext;
import rubik.util.Cubo;
import rubik.util.FlowController;
import rubik.util.Funciones;
import rubik.util.Partida;

/**
 * FXML Controller class
 *
 * @author
 */
public class ConfiguracionesController extends Controller implements Initializable {

    @FXML
    private JFXTextField txtNombre;
    @FXML
    private JFXToggleButton tbElegirPosiciones;
    @FXML
    private JFXToggleButton tbElegirColores;
    @FXML
    private FontAwesomeIconView faEyeDropper;
    @FXML
    private Label lbArriba;
    @FXML
    private JFXColorPicker cpArriba;
    @FXML
    private Label lbFrente;
    @FXML
    private JFXColorPicker cpFrente;
    @FXML
    private Label lbIzquierda;
    @FXML
    private JFXColorPicker cpIzquierda;
    @FXML
    private Label lbAbajo;
    @FXML
    private JFXColorPicker cpAbajo;
    @FXML
    private Label lbAtras;
    @FXML
    private JFXColorPicker cpAtras;
    @FXML
    private Label lbDerecha;
    @FXML
    private JFXColorPicker cpDerecha;
    @FXML
    private JFXButton btnGuardarConfi;

    private Boolean activaColor;
    private DrawerController drawer;
    private Cubo cube;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        inicializa();
        desactivaColor();
    }

    @Override
    public void initialize() {

    }

    public void inicializa() {
        // Boolean
        this.activaColor = false;

        //Color Pickers
        coloresPredeterminados();

        // Botones
        this.btnGuardarConfi.setText("");

        // Statico
        AppContext.getInstance().set("cc", this);
        this.drawer = (DrawerController) AppContext.getInstance().get("drawer");
    }

    /**
     * Guarda la configuración
     */
    @FXML
    public void guardarConfi() {
        FlowController.getInstance().delete("Juego");
        AppContext.getInstance().delete("cube");
        AppContext.getInstance().set("nombre", this.txtNombre.getText());
        if ((verificaColores(cpFrente.getValue()) + verificaColores(cpAtras.getValue()) + verificaColores(cpDerecha.getValue()) + verificaColores(cpIzquierda.getValue())
                + verificaColores(cpArriba.getValue()) + verificaColores(cpAbajo.getValue())) != 6) {
            coloresPredeterminados();
        }
        if ((Cubo) AppContext.getInstance().get("cube") == null) {
            this.cube = new Cubo(this.cpFrente.getValue(), this.cpAtras.getValue(), this.cpArriba.getValue(), this.cpAbajo.getValue(), this.cpDerecha.getValue(), this.cpIzquierda.getValue());
            AppContext.getInstance().set("cube", this.cube);
        } else {
            this.cube = (Cubo) AppContext.getInstance().get("cube");
        }
    }

    /**
     * Habilita la selección de colores
     *
     * @param event
     */
    @FXML
    private void elegirColores(ActionEvent event) {
        if (this.tbElegirColores.isSelected()) {
            activaColor();
        } else {
            desactivaColor();
        }
    }

    /**
     * Carga los colores predeterminados del cubo
     */
    public void coloresPredeterminados() {
        this.cpFrente.setValue(Color.valueOf("#1A237E"));
        this.cpAtras.setValue(Color.valueOf("#1B5E20"));
        this.cpArriba.setValue(Color.valueOf("#FFFF00"));
        this.cpAbajo.setValue(Color.valueOf("#FAFAFA"));
        this.cpIzquierda.setValue(Color.valueOf("#FF6F00"));
        this.cpDerecha.setValue(Color.valueOf("#b71c1c"));
    }

    /**
     * Verifica que no existan colores repetidos
     *
     * @param c
     * @return true si no hay colores repetidos
     */
    public Integer verificaColores(Color c) {
        Integer cont = 0;
        if (c.equals(cpArriba.getValue())) {
            cont++;
        }
        if (c.equals(cpAbajo.getValue())) {
            cont++;
        }
        if (c.equals(cpDerecha.getValue())) {
            cont++;
        }
        if (c.equals(cpIzquierda.getValue())) {
            cont++;
        }
        if (c.equals(cpArriba.getValue())) {
            cont++;
        }
        if (c.equals(cpAbajo.getValue())) {
            cont++;
        }
        return cont;
    }

    /**
     * Hace visibles los selectores de colores
     */
    public void activaColor() {
        this.faEyeDropper.setVisible(true);
        this.lbArriba.setVisible(true);
        this.cpArriba.setVisible(true);

        this.lbAbajo.setVisible(true);
        this.cpAbajo.setVisible(true);

        this.lbAtras.setVisible(true);
        this.cpAtras.setVisible(true);

        this.lbFrente.setVisible(true);
        this.cpFrente.setVisible(true);

        this.lbIzquierda.setVisible(true);
        this.cpIzquierda.setVisible(true);

        this.lbDerecha.setVisible(true);
        this.cpDerecha.setVisible(true);
    }

    /**
     * Hace invisibles los selectores de colores
     */
    public void desactivaColor() {
        this.faEyeDropper.setVisible(false);
        this.lbArriba.setVisible(false);
        this.cpArriba.setVisible(false);

        this.lbAbajo.setVisible(false);
        this.cpAbajo.setVisible(false);

        this.lbAtras.setVisible(false);
        this.cpAtras.setVisible(false);

        this.lbFrente.setVisible(false);
        this.cpFrente.setVisible(false);

        this.lbIzquierda.setVisible(false);
        this.cpIzquierda.setVisible(false);

        this.lbDerecha.setVisible(false);
        this.cpDerecha.setVisible(false);
    }

    /**
     * Manda a la pantalla de selección de colores le envia por parámetros los
     * colores seleccionados para el cubo
     *
     * @param event
     */
    @FXML
    private void elegirPosiciones(ActionEvent event) {
        if (tbElegirPosiciones.isSelected()) {
            if ((verificaColores(cpFrente.getValue()) + verificaColores(cpAtras.getValue()) + verificaColores(cpDerecha.getValue()) + verificaColores(cpIzquierda.getValue())
                    + verificaColores(cpArriba.getValue()) + verificaColores(cpAbajo.getValue())) != 6) {
                coloresPredeterminados();
            }
            AppContext.getInstance().set("frente", this.cpFrente.getValue());
            AppContext.getInstance().set("atras", this.cpAtras.getValue());
            AppContext.getInstance().set("izquierda", this.cpIzquierda.getValue());
            AppContext.getInstance().set("derecha", this.cpDerecha.getValue());
            AppContext.getInstance().set("arriba", this.cpArriba.getValue());
            AppContext.getInstance().set("abajo", this.cpAbajo.getValue());
            this.tbElegirPosiciones.setSelected(false);
            FlowController.getInstance().goView("ElegirPosiciones");
        }
    }

    @FXML
    private void cargar(ActionEvent event) throws IOException {
        FileChooser filechose = new FileChooser();
        filechose.setTitle("Buscar Partida");
        filechose.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Documents", "*.txt*")
        );

        File file = filechose.showOpenDialog(getStage());
        AppContext.getInstance().set("partida", cargaPartida(file.getAbsoluteFile()));
        elegirPosiciones(event);
    }

    /**
     * Cargar una partida
     *
     * @param file
     * @return Partida
     * @throws FileNotFoundException
     * @throws IOException
     */
    public Partida cargaPartida(File file) throws FileNotFoundException, IOException {
        FileInputStream fis = new FileInputStream(file);
        ObjectInputStream ois = new ObjectInputStream(fis);
        Partida aux = null;
        try {
            aux = (Partida) ois.readObject();
            return aux;
        } catch (ClassNotFoundException e) {
            Logger.getLogger(Partida.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }
    }
}
