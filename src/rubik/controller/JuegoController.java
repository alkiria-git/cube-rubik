/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rubik.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXToggleButton;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import rubik.util.AppContext;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import rubik.util.Cubo;
import rubik.util.Funciones;
import rubik.util.MovimientosOrdena;
import rubik.util.MovimientosUsuarios;
import rubik.util.Partida;
import rubik.util.TablaPosiciones;

/**
 * FXML Controller class
 *
 * @author
 */
public class JuegoController extends Controller implements Initializable {

    @FXML
    private AnchorPane apIzquierda;
    @FXML
    private AnchorPane apFrente;
    @FXML
    private AnchorPane apArriba;
    @FXML
    private AnchorPane apAbajo;
    @FXML
    private AnchorPane apDerecha;
    @FXML
    private AnchorPane apAtras;
    @FXML
    private Label lblCantMov;
    @FXML
    private JFXButton btnDesordena;
    @FXML
    private TableView<MovimientosUsuarios> tvMovimientos;
    @FXML
    private TableColumn<MovimientosUsuarios, String> tcMovimientos;
    @FXML
    private TableView<MovimientosOrdena> tvSolucion;
    @FXML
    private TableColumn<MovimientosOrdena, String> tcSolucion;
    @FXML
    private Label lblMinutos;
    @FXML
    private Label lblSegundos;
    @FXML
    private Label lblNombre;
    @FXML
    private JFXToggleButton tgSolucion;
    @FXML
    private JFXCheckBox solucionAutomatica;

    private Integer movimientos;
    private Label izquierda[][];
    private Label arriba[][];
    private Label frente[][];
    private Label abajo[][];
    private Label derecha[][];
    private Label atras[][];
    private Double temp = 0.0;
    private Cubo cube;
    private Boolean desordena;
    private Boolean tiempo;
    private Boolean terminado;
    private Boolean reiniciar;
    private Timer timer;
    private TablaPosiciones tp;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        inicializa();
    }

    @Override
    public void initialize() {

    }

    /**
     * Inicializa los componentes y atributos
     */
    public void inicializa() {
        this.tp = (TablaPosiciones) AppContext.getInstance().get("tp");
        this.desordena = false;
        this.tiempo = false;
        this.timer = new Timer();
        this.solucionAutomatica.setVisible(false);
        this.solucionAutomatica.setText("Solución automática");
        AppContext.getInstance().set("timer", this.timer);
        this.cube = (Cubo) AppContext.getInstance().get("cube");
        if (this.cube == null) {
            this.cube = new Cubo(Color.valueOf("#1A237E"), Color.valueOf("#1B5E20"), Color.valueOf("#FFFF00"), Color.valueOf("#FAFAFA"), Color.valueOf("#b71c1c"), Color.valueOf("#FF6F00"));
            AppContext.getInstance().set("cube", this.cube);
        }
        if ((String) AppContext.getInstance().get("nombre") == null) {
            this.lblNombre.setText("Anónimo");
        } else {
            this.lblNombre.setText((String) AppContext.getInstance().get("nombre"));
        }
        this.lblCantMov.setText("0");
        this.movimientos = 0;
        this.tcMovimientos.setCellValueFactory(new PropertyValueFactory<>("movimiento"));
        this.tcSolucion.setCellValueFactory(new PropertyValueFactory<>("movimiento"));
        creaCaras();
        actualizaCubo(false);
        this.reiniciar = false;
        this.terminado = true;
        cube.getMovimientos().clear();
        cube.getMovimientosOrdena().clear();
        cube.getMovimientosUsuario().clear();
        tvMovimientos.getItems().clear();
        tvSolucion.getItems().clear();
    }

    /**
     * Crea las caras visuales
     */
    public void creaCaras() {
        this.izquierda = new Label[3][3];
        this.arriba = new Label[3][3];
        this.frente = new Label[3][3];
        this.abajo = new Label[3][3];
        this.derecha = new Label[3][3];
        this.atras = new Label[3][3];

        int x = 0, y = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                this.izquierda[i][j] = new Label();
                this.izquierda[i][j].setLayoutX(x);
                this.izquierda[i][j].setLayoutY(y);
                this.izquierda[i][j].setPrefSize(30, 30);

                this.arriba[i][j] = new Label();
                this.arriba[i][j].setLayoutX(x);
                this.arriba[i][j].setLayoutY(y);
                this.arriba[i][j].setPrefSize(30, 30);

                this.frente[i][j] = new Label();
                this.frente[i][j].setLayoutX(x);
                this.frente[i][j].setLayoutY(y);
                this.frente[i][j].setPrefSize(30, 30);

                this.abajo[i][j] = new Label();
                this.abajo[i][j].setLayoutX(x);
                this.abajo[i][j].setLayoutY(y);
                this.abajo[i][j].setPrefSize(30, 30);

                this.derecha[i][j] = new Label();
                this.derecha[i][j].setLayoutX(x);
                this.derecha[i][j].setLayoutY(y);
                this.derecha[i][j].setPrefSize(30, 30);

                this.atras[i][j] = new Label();
                this.atras[i][j].setLayoutX(x);
                this.atras[i][j].setLayoutY(y);
                this.atras[i][j].setPrefSize(30, 30);

                this.arriba[i][j].setStyle(cube.Style(cube.colorHex(cube.getArriba())));
                this.izquierda[i][j].setStyle(cube.Style(cube.colorHex(cube.getIzquierda())));
                this.frente[i][j].setStyle(cube.Style(cube.colorHex(cube.getFrente())));
                this.abajo[i][j].setStyle(cube.Style(cube.colorHex(cube.getAbajo())));
                this.derecha[i][j].setStyle(cube.Style(cube.colorHex(cube.getDerecha())));
                this.atras[i][j].setStyle(cube.Style(cube.colorHex(cube.getAtras())));
                x += 31.5;
            }
            x = 0;
            y += 31.5;
        }

        //Indica las caras
        colorLetras();

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                this.apIzquierda.getChildren().add(this.izquierda[i][j]);
                this.apArriba.getChildren().add(this.arriba[i][j]);
                this.apFrente.getChildren().add(this.frente[i][j]);
                this.apAbajo.getChildren().add(this.abajo[i][j]);
                this.apDerecha.getChildren().add(this.derecha[i][j]);
                this.apAtras.getChildren().add(this.atras[i][j]);
            }
        }
    }

    /**
     * Inidica el color de las letras
     */
    public void colorLetras() {
        this.izquierda[1][1].setText("L");
        this.izquierda[1][1].setStyle(this.izquierda[1][1].getStyle() + "-fx-font-size: 21px;" + "-fx-font-weight: bold;" + "-fx-font-family: 'Bangers', cursive;"
                + "\n-fx-text-fill: " + cube.colorHex(cube.getRubik()[1][2][1].getDerecha()) + "-fx-alignment:center;");
        this.derecha[1][1].setText("R");
        this.derecha[1][1].setStyle(this.derecha[1][1].getStyle() + "-fx-font-size: 21px;" + "-fx-font-weight: bold;" + "-fx-font-family: 'Bangers', cursive;"
                + "\n-fx-text-fill: " + cube.colorHex(cube.getRubik()[1][0][1].getIzquierda()) + "-fx-alignment:center;");
        this.arriba[1][1].setText("U");
        this.arriba[1][1].setStyle(this.arriba[1][1].getStyle() + "-fx-font-size: 21px;" + "-fx-font-weight: bold;" + "-fx-font-family: 'Bangers', cursive;"
                + "\n-fx-text-fill: " + cube.colorHex(cube.getRubik()[2][1][1].getAbajo()) + "-fx-alignment:center;");
        this.abajo[1][1].setText("D");
        this.abajo[1][1].setStyle(this.abajo[1][1].getStyle() + "-fx-font-size: 21px;" + "-fx-font-weight: bold;" + "-fx-font-family: 'Bangers', cursive;"
                + "\n-fx-text-fill: " + cube.colorHex(cube.getRubik()[0][1][1].getArriba()) + "-fx-alignment:center;");
        this.frente[1][1].setText("F");
        this.frente[1][1].setStyle(this.frente[1][1].getStyle() + "-fx-font-size: 21px;" + "-fx-font-weight: bold;" + "-fx-font-family: 'Bangers', cursive;"
                + "\n-fx-text-fill: " + cube.colorHex(cube.getRubik()[1][1][2].getAtras()) + "-fx-alignment:center;");
        this.atras[1][1].setText("B");
        this.atras[1][1].setStyle(this.atras[1][1].getStyle() + "-fx-font-size: 21px;" + "-fx-font-weight: bold;" + "-fx-font-family: 'Bangers', cursive;"
                + "\n-fx-text-fill: " + cube.colorHex(cube.getRubik()[1][1][0].getFrente()) + "-fx-alignment:center;");
    }

    /**
     * Realiza el movimiento Up
     *
     * @param event
     */
    @FXML
    private void up(ActionEvent event) {
        cube.Up();
        if (!this.desordena) {
            cube.getMovimientosUsuario().add(new MovimientosUsuarios("U"));
        }
        actualizaCubo(reiniciar);
        cube.getMovimientos().add("U");
        this.movimientos++;
        this.lblCantMov.setText(Integer.toString(this.movimientos));
    }

    /**
     * Realiza el movimiento Down
     *
     * @param event
     */
    @FXML
    private void down(ActionEvent event) {
        cube.Down();
        if (!this.desordena) {
            cube.getMovimientosUsuario().add(new MovimientosUsuarios("D"));
        }
        actualizaCubo(reiniciar);
        cube.getMovimientos().add("D");
        this.movimientos++;
        this.lblCantMov.setText(Integer.toString(this.movimientos));
    }

    /**
     * Realiza el movimiento Right
     *
     * @param event
     */
    @FXML
    private void right(ActionEvent event) {
        cube.Rigth();
        if (!this.desordena) {
            cube.getMovimientosUsuario().add(new MovimientosUsuarios("R"));
        }
        actualizaCubo(reiniciar);
        cube.getMovimientos().add("R");
        this.movimientos++;
        this.lblCantMov.setText(Integer.toString(this.movimientos));
    }

    /**
     * Realiza el movimiento Left
     *
     * @param event
     */
    @FXML
    private void left(ActionEvent event) {
        cube.Left();
        if (!this.desordena) {
            cube.getMovimientosUsuario().add(new MovimientosUsuarios("L"));
        }
        actualizaCubo(reiniciar);
        cube.getMovimientos().add("L");
        this.movimientos++;
        this.lblCantMov.setText(Integer.toString(this.movimientos));
    }

    /**
     * Realiza el movimiento Front
     *
     * @param event
     */
    @FXML
    private void front(ActionEvent event) {
        cube.Front();
        if (!this.desordena) {
            cube.getMovimientosUsuario().add(new MovimientosUsuarios("F"));
        }
        actualizaCubo(reiniciar);
        cube.getMovimientos().add("F");
        this.movimientos++;
        this.lblCantMov.setText(Integer.toString(this.movimientos));
    }

    /**
     * Realiza el movimiento Back
     *
     * @param event
     */
    @FXML
    private void back(ActionEvent event) {
        cube.Back();
        if (!this.desordena) {
            cube.getMovimientosUsuario().add(new MovimientosUsuarios("B"));
        }
        actualizaCubo(reiniciar);
        cube.getMovimientos().add("B");
        this.movimientos++;
        this.lblCantMov.setText(Integer.toString(this.movimientos));
    }

    /**
     * Realiza el movimiento Up Prima
     *
     * @param event
     */
    @FXML
    private void up_prima(ActionEvent event) {
        cube.UpPrima();
        if (!this.desordena) {
            cube.getMovimientosUsuario().add(new MovimientosUsuarios("U'"));
        }
        actualizaCubo(reiniciar);
        cube.getMovimientos().add("U'");
        this.movimientos++;
        this.lblCantMov.setText(Integer.toString(this.movimientos));
    }

    /**
     * Realiza el movimiento Down Prima
     *
     * @param event
     */
    @FXML
    private void down_prima(ActionEvent event) {
        cube.DownPrima();
        if (!this.desordena) {
            cube.getMovimientosUsuario().add(new MovimientosUsuarios("D'"));
        }
        actualizaCubo(reiniciar);
        cube.getMovimientos().add("D'");
        this.movimientos++;
        this.lblCantMov.setText(Integer.toString(this.movimientos));
    }

    /**
     * Realiza el movimiento Right Prima
     *
     * @param event
     */
    @FXML
    private void right_prima(ActionEvent event) {
        cube.RigthPrima();
        if (!this.desordena) {
            cube.getMovimientosUsuario().add(new MovimientosUsuarios("R'"));
        }
        actualizaCubo(reiniciar);
        cube.getMovimientos().add("R'");
        this.movimientos++;
        this.lblCantMov.setText(Integer.toString(this.movimientos));
    }

    /**
     * Realiza el movimiento Left Prima
     *
     * @param event
     */
    @FXML
    private void left_prima(ActionEvent event) {
        cube.LeftPrima();
        if (!this.desordena) {
            cube.getMovimientosUsuario().add(new MovimientosUsuarios("L'"));
        }
        actualizaCubo(reiniciar);
        cube.getMovimientos().add("L'");
        this.movimientos++;
        this.lblCantMov.setText(Integer.toString(this.movimientos));
    }

    /**
     * Realiza el movimiento Front Prima
     *
     * @param event
     */
    @FXML
    private void front_prima(ActionEvent event) {
        cube.FrontPrima();
        if (!this.desordena) {
            cube.getMovimientosUsuario().add(new MovimientosUsuarios("F'"));
        }
        actualizaCubo(reiniciar);
        cube.getMovimientos().add("F'");
        this.movimientos++;
        this.lblCantMov.setText(Integer.toString(this.movimientos));
    }

    /**
     * Realiza el movimiento Back Prima
     *
     * @param event
     */
    @FXML
    private void back_prima(ActionEvent event) {
        cube.BackPrima();
        if (!this.desordena) {
            cube.getMovimientosUsuario().add(new MovimientosUsuarios("B'"));
        }
        actualizaCubo(reiniciar);
        cube.getMovimientos().add("B'");
        this.movimientos++;
        this.lblCantMov.setText(Integer.toString(this.movimientos));
    }

    /**
     * Realiza el movimiento eje derecha
     *
     * @param event
     */
    @FXML
    private void ejeDer(ActionEvent event) {
        cube.ejeDerecha();
        if (!this.desordena) {
            cube.getMovimientosUsuario().add(new MovimientosUsuarios("ER"));
        }
        actualizaCubo(reiniciar);
        cube.getMovimientos().add("ER");
        this.movimientos++;
        this.lblCantMov.setText(Integer.toString(this.movimientos));
    }

    /**
     * Realiza el movimiento eje arriba
     *
     * @param event
     */
    @FXML
    private void ejeArr(ActionEvent event) {
        cube.ejeArriba();
        if (!this.desordena) {
            cube.getMovimientosUsuario().add(new MovimientosUsuarios("EU"));
        }
        actualizaCubo(reiniciar);
        cube.getMovimientos().add("EU");
        this.movimientos++;
        this.lblCantMov.setText(Integer.toString(this.movimientos));
    }

    /**
     * Realiza el movimiento eje izquierda
     *
     * @param event
     */
    @FXML
    private void ejeIzq(ActionEvent event) {
        cube.ejeIzquierda();
        if (!this.desordena) {
            cube.getMovimientosUsuario().add(new MovimientosUsuarios("EL"));
        }
        actualizaCubo(reiniciar);
        cube.getMovimientos().add("EL");
        this.movimientos++;
        this.lblCantMov.setText(Integer.toString(this.movimientos));
    }

    /**
     * Realiza el movimiento eje abajo
     *
     * @param event
     */
    @FXML
    private void ejeAb(ActionEvent event) {
        cube.ejeAbajo();
        if (!this.desordena) {
            cube.getMovimientosUsuario().add(new MovimientosUsuarios("ED"));
        }
        actualizaCubo(reiniciar);
        cube.getMovimientos().add("ED");
        this.movimientos++;
        this.lblCantMov.setText(Integer.toString(this.movimientos));
    }

    /**
     * Reinicia el cubo
     *
     * @param e
     */
    @FXML
    private void reiniciar(ActionEvent e) {
        this.reiniciar = true;
        animacion(reiniciar);
        this.btnDesordena.setDisable(true);
        this.desordena = true;
        timer.cancel();
        Timer time = new Timer();
        TimerTask task = new TimerTask() {
            int second = cube.getMovimientos().size() - 1;

            @Override
            public void run() {
                Platform.runLater(() -> {
                    if (second < 0) {
                        cube.getMovimientos().clear();
                        cube.getMovimientosUsuario().clear();
                        cube.getMovimientosOrdena().clear();
                        cube.setFlag(false);
                        tvMovimientos.getItems().clear();
                        tvSolucion.getItems().clear();
                        lblCantMov.setText("0");
                        movimientos = 0;
                        btnDesordena.setDisable(false);
                        desordena = false;
                        time.cancel();
                        lblMinutos.setText("00");
                        lblSegundos.setText("00");
                        solucionAutomatica.setVisible(false);
                        reiniciar = false;
                    } else {
                        if (cube.getMovimientos().get(second).equalsIgnoreCase("U")) {
                            up_prima(e);
                        } else if (cube.getMovimientos().get(second).equalsIgnoreCase("U'")) {
                            up(e);
                        } else if (cube.getMovimientos().get(second).equalsIgnoreCase("D")) {
                            down_prima(e);
                        } else if (cube.getMovimientos().get(second).equalsIgnoreCase("D'")) {
                            down(e);
                        } else if (cube.getMovimientos().get(second).equalsIgnoreCase("F")) {
                            front_prima(e);
                        } else if (cube.getMovimientos().get(second).equalsIgnoreCase("F'")) {
                            front(e);
                        } else if (cube.getMovimientos().get(second).equalsIgnoreCase("B")) {
                            back_prima(e);
                        } else if (cube.getMovimientos().get(second).equalsIgnoreCase("B'")) {
                            back(e);
                        } else if (cube.getMovimientos().get(second).equalsIgnoreCase("L")) {
                            left_prima(e);
                        } else if (cube.getMovimientos().get(second).equalsIgnoreCase("L'")) {
                            left(e);
                        } else if (cube.getMovimientos().get(second).equalsIgnoreCase("R")) {
                            right_prima(e);
                        } else if (cube.getMovimientos().get(second).equalsIgnoreCase("R'")) {
                            right(e);
                        } else if (cube.getMovimientos().get(second).equalsIgnoreCase("ER")) {
                            ejeIzq(e);
                        } else if (cube.getMovimientos().get(second).equalsIgnoreCase("EL")) {
                            ejeDer(e);
                        } else if (cube.getMovimientos().get(second).equalsIgnoreCase("EU")) {
                            ejeAb(e);
                        } else if (cube.getMovimientos().get(second).equalsIgnoreCase("ED")) {
                            ejeArr(e);
                        }
                        movimientos = 0;
                        lblCantMov.setText("0");
                        second--;
                    }
                });
            }
        };
        time.schedule(task, 0, 50);
        tiempo();
    }

    /**
     * Desordena el cubo aleatoriamente
     *
     * @param event
     */
    @FXML
    private void desordena(ActionEvent event) {
        this.desordena = true;
        cube.desordenaCubo(16);
        actualizaCubo(reiniciar);
        this.desordena = false;
        this.tgSolucion.setSelected(false);
        this.tvSolucion.getItems().clear();
        this.cube.getOrdena().clear();
        this.cube.getMovimientosOrdena().clear();
        this.solucionAutomatica.setVisible(false);
    }

    @FXML
    private void guardarJuego(ActionEvent event) throws IOException {
        this.timer.cancel();
        Partida partida = new Partida(this.lblNombre.getText(),
                this.lblCantMov.getText(), this.lblMinutos.getText(), this.lblSegundos.getText(),
                cara(frente), cara(atras), cara(derecha), cara(izquierda), cara(arriba), cara(abajo));

        File file = new File(this.lblNombre.getText() + ".txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oss = new ObjectOutputStream(fos);

        oss.writeObject(partida);
        oss.flush();
        fos.close();
        oss.close();
    }

    /**
     * Lleva el tiempo del juego
     */
    public void tiempo() {
        TimerTask task = new TimerTask() {
            int second = 0;
            int minute = 0;

            @Override
            public void run() {
                Platform.runLater(() -> {
                    if (tiempo) {
                        if (second > 59) {
                            second = 0;
                            minute++;
                            lblSegundos.setText(Integer.toString(second));
                            lblMinutos.setText(Integer.toString(minute));
                        } else {
                            second++;
                            lblSegundos.setText(Integer.toString(second));
                        }
                    } else {
                        timer.cancel();
                    }
                });
            }
        };
        timer.schedule(task, 0, 1000);
    }

    /**
     * Actualiza el cubo logico con los datos de la matriz tridimencional
     *
     * @param reinicia
     */
    public void actualizaCubo(Boolean reinicia) {
        //Frente
        actFrente();
        //Atras
        actAtras();
        //Derecha
        actDerecha();
        //Izquierda
        actIzquierda();
        //Arriba
        actArriba();
        //Abajo
        actAbajo();

        if (!tiempo) {
            tiempo = true;
            tiempo();
        }
        colorLetras();
        if (cube.getMovimientosUsuario().size() > 0) {
            cargaTablaMovimientos();
        }
        if (!reinicia) {
            this.terminado = true;
        } else {
            this.terminado = false;
        }
        terminado();
    }

    /**
     * Toma los datos de la matriz tridimensional para actualizar el frente
     */
    public void actFrente() {
        this.frente[0][0].setStyle(cube.Style(cube.colorHex(cube.getRubik()[0][0][0].getFrente())));
        this.frente[0][1].setStyle(cube.Style(cube.colorHex(cube.getRubik()[0][1][0].getFrente())));
        this.frente[0][2].setStyle(cube.Style(cube.colorHex(cube.getRubik()[0][2][0].getFrente())));
        this.frente[1][0].setStyle(cube.Style(cube.colorHex(cube.getRubik()[1][0][0].getFrente())));
        this.frente[1][1].setStyle(cube.Style(cube.colorHex(cube.getRubik()[1][1][0].getFrente())));
        this.frente[1][2].setStyle(cube.Style(cube.colorHex(cube.getRubik()[1][2][0].getFrente())));
        this.frente[2][0].setStyle(cube.Style(cube.colorHex(cube.getRubik()[2][0][0].getFrente())));
        this.frente[2][1].setStyle(cube.Style(cube.colorHex(cube.getRubik()[2][1][0].getFrente())));
        this.frente[2][2].setStyle(cube.Style(cube.colorHex(cube.getRubik()[2][2][0].getFrente())));
    }

    /**
     * Toma los datos de la matriz tridimensional para actualizar atras
     */
    public void actAtras() {
        this.atras[0][0].setStyle(cube.Style(cube.colorHex(cube.getRubik()[0][2][2].getAtras())));
        this.atras[0][1].setStyle(cube.Style(cube.colorHex(cube.getRubik()[0][1][2].getAtras())));
        this.atras[0][2].setStyle(cube.Style(cube.colorHex(cube.getRubik()[0][0][2].getAtras())));
        this.atras[1][0].setStyle(cube.Style(cube.colorHex(cube.getRubik()[1][2][2].getAtras())));
        this.atras[1][1].setStyle(cube.Style(cube.colorHex(cube.getRubik()[1][1][2].getAtras())));
        this.atras[1][2].setStyle(cube.Style(cube.colorHex(cube.getRubik()[1][0][2].getAtras())));
        this.atras[2][0].setStyle(cube.Style(cube.colorHex(cube.getRubik()[2][2][2].getAtras())));
        this.atras[2][1].setStyle(cube.Style(cube.colorHex(cube.getRubik()[2][1][2].getAtras())));
        this.atras[2][2].setStyle(cube.Style(cube.colorHex(cube.getRubik()[2][0][2].getAtras())));
    }

    /**
     * Toma los datos de la matriz tridimensional para actualizar derecha
     */
    public void actDerecha() {
        this.derecha[0][0].setStyle(cube.Style(cube.colorHex(cube.getRubik()[0][2][0].getDerecha())));
        this.derecha[0][1].setStyle(cube.Style(cube.colorHex(cube.getRubik()[0][2][1].getDerecha())));
        this.derecha[0][2].setStyle(cube.Style(cube.colorHex(cube.getRubik()[0][2][2].getDerecha())));
        this.derecha[1][0].setStyle(cube.Style(cube.colorHex(cube.getRubik()[1][2][0].getDerecha())));
        this.derecha[1][1].setStyle(cube.Style(cube.colorHex(cube.getRubik()[1][2][1].getDerecha())));
        this.derecha[1][2].setStyle(cube.Style(cube.colorHex(cube.getRubik()[1][2][2].getDerecha())));
        this.derecha[2][0].setStyle(cube.Style(cube.colorHex(cube.getRubik()[2][2][0].getDerecha())));
        this.derecha[2][1].setStyle(cube.Style(cube.colorHex(cube.getRubik()[2][2][1].getDerecha())));
        this.derecha[2][2].setStyle(cube.Style(cube.colorHex(cube.getRubik()[2][2][2].getDerecha())));
    }

    /**
     * Toma los datos de la matriz tridimensional para actualizar izquierda
     */
    public void actIzquierda() {
        this.izquierda[0][0].setStyle(cube.Style(cube.colorHex(cube.getRubik()[0][0][2].getIzquierda())));
        this.izquierda[0][1].setStyle(cube.Style(cube.colorHex(cube.getRubik()[0][0][1].getIzquierda())));
        this.izquierda[0][2].setStyle(cube.Style(cube.colorHex(cube.getRubik()[0][0][0].getIzquierda())));
        this.izquierda[1][0].setStyle(cube.Style(cube.colorHex(cube.getRubik()[1][0][2].getIzquierda())));
        this.izquierda[1][1].setStyle(cube.Style(cube.colorHex(cube.getRubik()[1][0][1].getIzquierda())));
        this.izquierda[1][2].setStyle(cube.Style(cube.colorHex(cube.getRubik()[1][0][0].getIzquierda())));
        this.izquierda[2][0].setStyle(cube.Style(cube.colorHex(cube.getRubik()[2][0][2].getIzquierda())));
        this.izquierda[2][1].setStyle(cube.Style(cube.colorHex(cube.getRubik()[2][0][1].getIzquierda())));
        this.izquierda[2][2].setStyle(cube.Style(cube.colorHex(cube.getRubik()[2][0][0].getIzquierda())));
    }

    /**
     * Toma los datos de la matriz tridimensional para actualizar arriba
     */
    public void actArriba() {
        this.arriba[0][0].setStyle(cube.Style(cube.colorHex(cube.getRubik()[0][0][2].getArriba())));
        this.arriba[0][1].setStyle(cube.Style(cube.colorHex(cube.getRubik()[0][1][2].getArriba())));
        this.arriba[0][2].setStyle(cube.Style(cube.colorHex(cube.getRubik()[0][2][2].getArriba())));
        this.arriba[1][0].setStyle(cube.Style(cube.colorHex(cube.getRubik()[0][0][1].getArriba())));
        this.arriba[1][1].setStyle(cube.Style(cube.colorHex(cube.getRubik()[0][1][1].getArriba())));
        this.arriba[1][2].setStyle(cube.Style(cube.colorHex(cube.getRubik()[0][2][1].getArriba())));
        this.arriba[2][0].setStyle(cube.Style(cube.colorHex(cube.getRubik()[0][0][0].getArriba())));
        this.arriba[2][1].setStyle(cube.Style(cube.colorHex(cube.getRubik()[0][1][0].getArriba())));
        this.arriba[2][2].setStyle(cube.Style(cube.colorHex(cube.getRubik()[0][2][0].getArriba())));
    }

    /**
     * Toma los datos de la matriz tridimensional para actualizar abajo
     */
    public void actAbajo() {
        this.abajo[0][0].setStyle(cube.Style(cube.colorHex(cube.getRubik()[2][0][0].getAbajo())));
        this.abajo[0][1].setStyle(cube.Style(cube.colorHex(cube.getRubik()[2][1][0].getAbajo())));
        this.abajo[0][2].setStyle(cube.Style(cube.colorHex(cube.getRubik()[2][2][0].getAbajo())));
        this.abajo[1][0].setStyle(cube.Style(cube.colorHex(cube.getRubik()[2][0][1].getAbajo())));
        this.abajo[1][1].setStyle(cube.Style(cube.colorHex(cube.getRubik()[2][1][1].getAbajo())));
        this.abajo[1][2].setStyle(cube.Style(cube.colorHex(cube.getRubik()[2][2][1].getAbajo())));
        this.abajo[2][0].setStyle(cube.Style(cube.colorHex(cube.getRubik()[2][0][2].getAbajo())));
        this.abajo[2][1].setStyle(cube.Style(cube.colorHex(cube.getRubik()[2][1][2].getAbajo())));
        this.abajo[2][2].setStyle(cube.Style(cube.colorHex(cube.getRubik()[2][2][2].getAbajo())));
    }

    /**
     * Verifica si el cubo esta terminado
     */
    public void terminado() {
        if (cube.terminado() && terminado) {
            if (!lblCantMov.getText().equalsIgnoreCase("0") && !lblMinutos.getText().equalsIgnoreCase("0") && !lblSegundos.getText().equalsIgnoreCase("0")) {
                Integer puntos = Integer.parseInt(lblCantMov.getText()) / (Integer.parseInt(lblMinutos.getText()) * 60 + Integer.parseInt(lblSegundos.getText()));
                this.tp.getTabla().add(new TablaPosiciones(this.lblNombre.getText(), puntos, lblCantMov.getText(), lblMinutos.getText() + ":" + lblSegundos.getText()));
                AppContext.getInstance().set("tp", this.tp);
                Funciones.mensajeExito("Felicidades lograste resolver el puzzle!");
            }
        }
    }

    /**
     * Carga los movimientos del usuario en la tabla
     */
    public void cargaTablaMovimientos() {
        this.tvMovimientos.setItems(this.cube.getMovimientosUsuario());
    }

    /**
     * Muestra la solucion en la tabla de solucion
     *
     * @param event
     */
    @FXML
    private void mostrarSolucion(ActionEvent event) {
        if (tgSolucion.isSelected()) {
            this.solucionAutomatica.setVisible(true);
            cube.algoritmoSolucion();
            cube.copyArrayListToObservableList();
            this.tvSolucion.setItems(cube.getOrdena());
            quitaSolucion();
            actualizaCubo(false);
        } else {
            this.cube.getMovimientosOrdena().clear();
            this.cube.getOrdena().clear();
            this.tvSolucion.getItems().clear();
        }
    }

    /**
     * Soluciona el cubo automaticamente
     *
     * @param event
     */
    @FXML
    private void solucion(ActionEvent event) {
        this.solucionAutomatica.setSelected(false);
        this.solucionAutomatica.setVisible(false);
        this.cube.getMovimientosOrdena().clear();
        cube.getMovimientos().clear();
        cube.algoritmoSolucion();
        cube.getMovimientos().clear();
        actualizaCubo(true);
    }

    /**
     * Animacion al reiniciar el cubo
     *
     * @param reinicia
     */
    public void animacion(Boolean reinicia) {
        if (this.cube.getMovimientos().isEmpty() && !this.cube.getMovimientosOrdena().isEmpty()) {
            for (int i = 0; i < this.cube.getMovimientosOrdena().size(); i++) {
                this.cube.getMovimientos().add(this.cube.getMovimientosOrdena().get(i));
            }
        } else if (this.cube.getMovimientos().isEmpty() && this.cube.getMovimientosOrdena().isEmpty()) {
            this.cube.getMovimientosOrdena().clear();
            cube.algoritmoSolucion();
            actualizaCubo(reinicia);
        }
    }

    /**
     * Quita los movimientos solucion para ver animacion
     */
    public void quitaSolucion() {
        for (int i = cube.getMovimientosOrdena().size() - 1; i > -1; i--) {
            if (cube.getMovimientosOrdena().get(i).equalsIgnoreCase("Eje Derecha") || cube.getMovimientosOrdena().get(i).equalsIgnoreCase("eD")) {
                cube.ejeIzquierda();
            } else if (cube.getMovimientosOrdena().get(i).equalsIgnoreCase("Eje Izquierda") || cube.getMovimientosOrdena().get(i).equalsIgnoreCase("eI")) {
                cube.ejeDerecha();
            } else if (cube.getMovimientosOrdena().get(i).equalsIgnoreCase("U")) {
                cube.UpPrima();
            } else if (cube.getMovimientosOrdena().get(i).equalsIgnoreCase("U'")) {
                cube.Up();
            } else if (cube.getMovimientosOrdena().get(i).equalsIgnoreCase("D")) {
                cube.DownPrima();
            } else if (cube.getMovimientosOrdena().get(i).equalsIgnoreCase("D'")) {
                cube.Down();
            } else if (cube.getMovimientosOrdena().get(i).equalsIgnoreCase("F")) {
                cube.FrontPrima();
            } else if (cube.getMovimientosOrdena().get(i).equalsIgnoreCase("F'")) {
                cube.Front();
            } else if (cube.getMovimientosOrdena().get(i).equalsIgnoreCase("B")) {
                cube.BackPrima();
            } else if (cube.getMovimientosOrdena().get(i).equalsIgnoreCase("B'")) {
                cube.Back();
            } else if (cube.getMovimientosOrdena().get(i).equalsIgnoreCase("R")) {
                cube.RigthPrima();
            } else if (cube.getMovimientosOrdena().get(i).equalsIgnoreCase("R'")) {
                cube.Rigth();
            } else if (cube.getMovimientosOrdena().get(i).equalsIgnoreCase("L")) {
                cube.LeftPrima();
            } else if (cube.getMovimientosOrdena().get(i).equalsIgnoreCase("L'")) {
                cube.Left();
            } else {
                System.out.println("Movimiento no definido: " + cube.getMovimientosOrdena().get(i));
            }
        }
    }

    public String[][] cara(Label[][] face) {
        String cara[][] = new String[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                cara[i][j] = face[i][j].getStyle().substring(0, 30);
            }
        }
        return cara;
    }
}
