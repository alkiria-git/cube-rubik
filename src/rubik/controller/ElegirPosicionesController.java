/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rubik.controller;

import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import rubik.util.Cubo;
import rubik.util.AppContext;
import rubik.util.FlowController;
import rubik.util.Funciones;
import rubik.util.Partida;

/**
 * FXML Controller class
 *
 * @author Eduardo Salas Cerdas
 */
public class ElegirPosicionesController extends Controller implements Initializable {

    @FXML
    private AnchorPane apFrente;
    @FXML
    private AnchorPane apIzquierda;
    @FXML
    private AnchorPane apArriba;
    @FXML
    private AnchorPane apAbajo;
    @FXML
    private AnchorPane apDerecha;
    @FXML
    private AnchorPane apAtras;
    @FXML
    private Label ColorF;
    @FXML
    private Label ColorB;
    @FXML
    private Label ColorR;
    @FXML
    private Label ColorL;
    @FXML
    private Label ColorU;
    @FXML
    private Label ColorD;
    @FXML
    private Label ColorLimpia;
    @FXML
    private JFXButton btnGuardar;

    private ArrayList<Label> frente;
    private ArrayList<Label> izquierda;
    private ArrayList<Label> arriba;
    private ArrayList<Label> abajo;
    private ArrayList<Label> derecha;
    private ArrayList<Label> atras;
    private ArrayList<String> estilo;
    private Cubo cube;
    private String front;
    private String back;
    private String left;
    private String right;
    private String up;
    private String down;
    private Boolean limpia;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        inicializa();
    }

    @Override
    public void initialize() {

    }

    /**
     * Inicializa los componentes y variables
     */
    public void inicializa() {
        this.estilo = new ArrayList();
        this.cube = (Cubo) AppContext.getInstance().get("cube");
        if (this.cube == null) {
            cube = new Cubo();
            AppContext.getInstance().set("cube", this.cube);
        }
        cube = new Cubo((Color) AppContext.getInstance().get("frente"), (Color) AppContext.getInstance().get("atras"),
                (Color) AppContext.getInstance().get("arriba"), (Color) AppContext.getInstance().get("abajo"),
                (Color) AppContext.getInstance().get("derecha"), (Color) AppContext.getInstance().get("izquierda"));
        creaCaras();
        this.front = Integer.toHexString(((Color) AppContext.getInstance().get("frente")).hashCode()).substring(0, 6).toUpperCase();
        this.back = Integer.toHexString(((Color) AppContext.getInstance().get("atras")).hashCode()).substring(0, 6).toUpperCase();
        this.left = Integer.toHexString(((Color) AppContext.getInstance().get("izquierda")).hashCode()).substring(0, 6).toUpperCase();
        this.right = Integer.toHexString(((Color) AppContext.getInstance().get("derecha")).hashCode()).substring(0, 6).toUpperCase();
        this.up = Integer.toHexString(((Color) AppContext.getInstance().get("arriba")).hashCode()).substring(0, 6).toUpperCase();
        this.down = Integer.toHexString(((Color) AppContext.getInstance().get("abajo")).hashCode()).substring(0, 6).toUpperCase();

        this.limpia = false;
        //Colores
        this.ColorF.setText("");
        this.ColorB.setText("");
        this.ColorR.setText("");
        this.ColorL.setText("");
        this.ColorU.setText("");
        this.ColorD.setText("");
        this.ColorLimpia.setText("");
        this.ColorF.setStyle(style((Color) AppContext.getInstance().get("frente")) + "-fx-border-color: #CFB53B;");
        this.ColorB.setStyle(style((Color) AppContext.getInstance().get("atras")) + "-fx-border-color: #CFB53B;");
        this.ColorR.setStyle(style((Color) AppContext.getInstance().get("derecha")) + "-fx-border-color: #CFB53B;");
        this.ColorL.setStyle(style((Color) AppContext.getInstance().get("izquierda")) + "-fx-border-color: #CFB53B;");
        this.ColorU.setStyle(style((Color) AppContext.getInstance().get("arriba")) + "-fx-border-color: #CFB53B;");
        this.ColorD.setStyle(style((Color) AppContext.getInstance().get("abajo")) + "-fx-border-color: #CFB53B;");
        this.ColorLimpia.setStyle(style(Color.valueOf("#C0C0C0")) + "-fx-border-color: #CFB53B;");

        //Mouse
        mouseEvent();
        aplicaEstilo();
        
        if ((Partida) AppContext.getInstance().get("partida") != null) {
            cargaDatos();
        }
    }

    /**
     * Crea las caras visuales
     */
    public void creaCaras() {
        this.izquierda = new ArrayList();
        this.arriba = new ArrayList();
        this.frente = new ArrayList();
        this.abajo = new ArrayList();
        this.derecha = new ArrayList();
        this.atras = new ArrayList();

        int x = 0, y = 0, k = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                this.izquierda.add(new Label());
                this.izquierda.get(k).setLayoutX(x);
                this.izquierda.get(k).setLayoutY(y);
                this.izquierda.get(k).setPrefSize(30, 30);

                this.arriba.add(new Label());
                this.arriba.get(k).setLayoutX(x);
                this.arriba.get(k).setLayoutY(y);
                this.arriba.get(k).setPrefSize(30, 30);

                this.frente.add(new Label());
                this.frente.get(k).setLayoutX(x);
                this.frente.get(k).setLayoutY(y);
                this.frente.get(k).setPrefSize(30, 30);

                this.abajo.add(new Label());
                this.abajo.get(k).setLayoutX(x);
                this.abajo.get(k).setLayoutY(y);
                this.abajo.get(k).setPrefSize(30, 30);

                this.derecha.add(new Label());
                this.derecha.get(k).setLayoutX(x);
                this.derecha.get(k).setLayoutY(y);
                this.derecha.get(k).setPrefSize(30, 30);

                this.atras.add(new Label());
                this.atras.get(k).setLayoutX(x);
                this.atras.get(k).setLayoutY(y);
                this.atras.get(k).setPrefSize(30, 30);

                this.arriba.get(k).setStyle("-fx-border-color: #CFB53B;");
                this.izquierda.get(k).setStyle("-fx-border-color: #CFB53B;");
                this.frente.get(k).setStyle("-fx-border-color: #CFB53B;");
                this.abajo.get(k).setStyle("-fx-border-color: #CFB53B;");
                this.derecha.get(k).setStyle("-fx-border-color: #CFB53B;");
                this.atras.get(k).setStyle("-fx-border-color: #CFB53B;");

                k++;
                x += 31.5;
            }
            x = 0;
            y += 31.5;
        }
        k = 0;
        for (int i = 0; i < 9; i++) {
            this.apIzquierda.getChildren().add(this.izquierda.get(k));
            this.apArriba.getChildren().add(this.arriba.get(k));
            this.apFrente.getChildren().add(this.frente.get(k));
            this.apAbajo.getChildren().add(this.abajo.get(k));
            this.apDerecha.getChildren().add(this.derecha.get(k));
            this.apAtras.getChildren().add(this.atras.get(k));
            k++;
        }
        colorLetras();
    }

    /**
     * Activa los mouseEvents
     */
    public void mouseEvent() {
        ColorF.setOnMouseClicked((e) -> {
            estilo.clear();
            limpia = false;
            estilo.add(ColorF.getStyle());
            aplicaEstilo();
        });
        ColorB.setOnMouseClicked((e) -> {
            estilo.clear();
            limpia = false;
            estilo.add(ColorB.getStyle());
            aplicaEstilo();
        });
        ColorR.setOnMouseClicked((e) -> {
            estilo.clear();
            limpia = false;
            estilo.add(ColorR.getStyle());
            aplicaEstilo();
        });
        ColorL.setOnMouseClicked((e) -> {
            estilo.clear();
            limpia = false;
            estilo.add(ColorL.getStyle());
            aplicaEstilo();
        });
        ColorU.setOnMouseClicked((e) -> {
            estilo.clear();
            limpia = false;
            estilo.add(ColorU.getStyle());
            aplicaEstilo();
        });
        ColorD.setOnMouseClicked((e) -> {
            estilo.clear();
            limpia = false;
            estilo.add(ColorD.getStyle());
            aplicaEstilo();
        });
        ColorLimpia.setOnMouseClicked((e) -> {
            estilo.clear();
            limpia = true;
            estilo.add(ColorLimpia.getStyle());
        });
    }

    /**
     * Aplica los estilos a la posicion seleccionada
     */
    public void aplicaEstilo() {
        clickFrente();
        clickAtras();
        clickDerecha();
        clickIzquierda();
        clickArriba();
        clickAbajo();
    }

    /**
     * Detecta la pieza clickeada en el frente
     */
    public void clickFrente() {
        if (!estilo.isEmpty() || limpia) {
            frente.get(0).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    frente.get(0).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    frente.get(0).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            frente.get(1).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    frente.get(1).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    frente.get(1).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            frente.get(2).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    frente.get(2).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    frente.get(2).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            frente.get(3).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    frente.get(3).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    frente.get(3).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            frente.get(5).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    frente.get(5).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    frente.get(5).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            frente.get(6).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    frente.get(6).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    frente.get(6).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            frente.get(7).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    frente.get(7).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    frente.get(7).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            frente.get(8).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    frente.get(8).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    frente.get(8).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
        }
    }

    /**
     * Detecta pieza clickeada en el panel trasero
     */
    public void clickAtras() {
        if (!estilo.isEmpty() || limpia) {
            atras.get(0).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    atras.get(0).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    atras.get(0).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            atras.get(1).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    atras.get(1).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    atras.get(1).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            atras.get(2).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    atras.get(2).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    atras.get(2).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            atras.get(3).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    atras.get(3).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    atras.get(3).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            atras.get(5).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    atras.get(5).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    atras.get(5).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            atras.get(6).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    atras.get(6).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    atras.get(6).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            atras.get(7).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    atras.get(7).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    atras.get(7).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            atras.get(8).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    atras.get(8).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    atras.get(8).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
        }
    }

    /**
     * Detecta la pieza clickeada en el panel derecho
     */
    public void clickDerecha() {
        if (!estilo.isEmpty() || limpia) {
            derecha.get(0).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    derecha.get(0).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    derecha.get(0).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            derecha.get(1).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    derecha.get(1).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    derecha.get(1).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            derecha.get(2).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    derecha.get(2).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    derecha.get(2).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            derecha.get(3).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    derecha.get(3).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    derecha.get(3).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            derecha.get(5).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    derecha.get(5).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    derecha.get(5).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            derecha.get(6).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    derecha.get(6).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    derecha.get(6).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            derecha.get(7).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    derecha.get(7).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    derecha.get(7).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            derecha.get(8).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    derecha.get(8).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    derecha.get(8).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
        }
    }

    /**
     * Detecta la pieza clickeada en el panel izquierdo
     */
    public void clickIzquierda() {
        if (!estilo.isEmpty() || limpia) {
            izquierda.get(0).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    izquierda.get(0).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    izquierda.get(0).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            izquierda.get(1).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    izquierda.get(1).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    izquierda.get(1).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            izquierda.get(2).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    izquierda.get(2).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    izquierda.get(2).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            izquierda.get(3).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    izquierda.get(3).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    izquierda.get(3).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            izquierda.get(5).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    izquierda.get(5).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    izquierda.get(5).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            izquierda.get(6).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    izquierda.get(6).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    izquierda.get(6).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            izquierda.get(7).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    izquierda.get(7).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    izquierda.get(7).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            izquierda.get(8).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    izquierda.get(8).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    izquierda.get(8).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
        }
    }

    /**
     * Detecta la pieza clickeada en el panel de arriba
     */
    public void clickArriba() {
        if (!estilo.isEmpty() || limpia) {
            arriba.get(0).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    arriba.get(0).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    arriba.get(0).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            arriba.get(1).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    arriba.get(1).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    arriba.get(1).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            arriba.get(2).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    arriba.get(2).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    arriba.get(2).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            arriba.get(3).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    arriba.get(3).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    arriba.get(3).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            arriba.get(5).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    arriba.get(5).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    arriba.get(5).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            arriba.get(6).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    arriba.get(6).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    arriba.get(6).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            arriba.get(7).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    arriba.get(7).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    arriba.get(7).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            arriba.get(8).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    arriba.get(8).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    arriba.get(8).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
        }
    }

    /**
     * Detecta la pieza clickeada en el panel de abajo
     */
    public void clickAbajo() {
        if (!estilo.isEmpty() || limpia) {
            abajo.get(0).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    abajo.get(0).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    abajo.get(0).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            abajo.get(1).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    abajo.get(1).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    abajo.get(1).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            abajo.get(2).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    abajo.get(2).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    abajo.get(2).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            abajo.get(3).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    abajo.get(3).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    abajo.get(3).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            abajo.get(5).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    abajo.get(5).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    abajo.get(5).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            abajo.get(6).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    abajo.get(6).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    abajo.get(6).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            abajo.get(7).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    abajo.get(7).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    abajo.get(7).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
            abajo.get(8).setOnMouseClicked((e) -> {
                if (limpia || estilo.isEmpty()) {
                    abajo.get(8).setStyle(ColorLimpia.getStyle());
                    limpia = false;
                } else {
                    abajo.get(8).setStyle(estilo.get(0));
                }
                estilo.clear();
            });
        }
    }

    /**
     * Aplica un estilo a un label
     *
     * @param label
     * @param style
     */
    public void estilo(Label label, String style) {
        label.setStyle(style + label.getStyle());
    }

    /**
     * Colorea las piezas centrales y les define una letra para mejor ubicación
     */
    public void colorLetras() {
        this.izquierda.get(4).setText("L");
        this.izquierda.get(4).setStyle(this.izquierda.get(4).getStyle() + "-fx-font-size: 15px;" + "-fx-font-weight: bold;" + "-fx-font-family: 'Bangers', cursive;"
                + "\n-fx-text-fill: #CFB53B;" + "-fx-alignment:center;");
        this.derecha.get(4).setText("R");
        this.derecha.get(4).setStyle(this.derecha.get(4).getStyle() + "-fx-font-size: 15px;" + "-fx-font-weight: bold;" + "-fx-font-family: 'Bangers', cursive;"
                + "\n-fx-text-fill: #CFB53B;" + "-fx-alignment:center;");
        this.arriba.get(4).setText("U");
        this.arriba.get(4).setStyle(this.arriba.get(4).getStyle() + "-fx-font-size: 15px;" + "-fx-font-weight: bold;" + "-fx-font-family: 'Bangers', cursive;"
                + "\n-fx-text-fill: #CFB53B;" + "-fx-alignment:center;");
        this.abajo.get(4).setText("D");
        this.abajo.get(4).setStyle(this.abajo.get(4).getStyle() + "-fx-font-size: 15px;" + "-fx-font-weight: bold;" + "-fx-font-family: 'Bangers', cursive;"
                + "\n-fx-text-fill: #CFB53B;" + "-fx-alignment:center;");
        this.frente.get(4).setText("F");
        this.frente.get(4).setStyle(this.frente.get(4).getStyle() + "-fx-font-size: 15px;" + "-fx-font-weight: bold;" + "-fx-font-family: 'Bangers', cursive;"
                + "\n-fx-text-fill: #CFB53B;" + "-fx-alignment:center;");
        this.atras.get(4).setText("B");
        this.atras.get(4).setStyle(this.atras.get(4).getStyle() + "-fx-font-size: 15px;" + "-fx-font-weight: bold;" + "-fx-font-family: 'Bangers', cursive;"
                + "\n-fx-text-fill: #CFB53B;" + "-fx-alignment:center;");

        this.frente.get(4).setStyle(style((Color) AppContext.getInstance().get("frente")) + "\n" + this.frente.get(4).getStyle());
        this.atras.get(4).setStyle(style((Color) AppContext.getInstance().get("atras")) + "\n" + this.atras.get(4).getStyle());
        this.izquierda.get(4).setStyle(style((Color) AppContext.getInstance().get("izquierda")) + "\n" + this.izquierda.get(4).getStyle());
        this.derecha.get(4).setStyle(style((Color) AppContext.getInstance().get("derecha")) + "\n" + this.derecha.get(4).getStyle());
        this.arriba.get(4).setStyle(style((Color) AppContext.getInstance().get("arriba")) + "\n" + this.arriba.get(4).getStyle());
        this.abajo.get(4).setStyle(style((Color) AppContext.getInstance().get("abajo")) + "\n" + this.abajo.get(4).getStyle());
    }

    /**
     * Detecta el color de fondo de un label
     *
     * @param label
     * @return color label
     */
    public String color(Label label) {
        return label.getStyle().substring(22, 29);
    }

    /**
     * Convierte un color a hexadecimal para usarlo en el .setStyle()
     *
     * @param color
     * @return style
     */
    public String style(Color color) {
        return "-fx-background-color: #" + Integer.toHexString(color.hashCode()).substring(0, 6).toUpperCase() + ";";
    }

    /**
     * Guarda el cubo si es posible de armar y vuelve a la pantalla de
     * configuraciones
     */
    @FXML
    public void guardar() {
        if (!lleno()) {
            Funciones.mensajeError("Al cubo le faltan piezas.\nVerifique e intente nuevamente");
        } else {
            creaCuboLogico();
            cube.algoritmoSolucion();
            if (!cube.terminado()) {
                Funciones.mensajeError("El cubo es imposible de armar.\nVerifique e intente nuevamente");
            } else {
                quitaSolucion();
                creaCuboLogico();
                AppContext.getInstance().set("cube", this.cube);
                FlowController.getInstance().goView("Configuraciones");
            }
        }
    }

    /**
     * Quita la solución del cubo al terminar de evaluar si es posible de armar
     */
    public void quitaSolucion() {
        for (int i = cube.getMovimientosOrdena().size() - 1; i > -1; i--) {
            if (cube.getMovimientosOrdena().get(i).equalsIgnoreCase("Eje Derecha") || cube.getMovimientosOrdena().get(i).equalsIgnoreCase("eD")) {
                cube.ejeIzquierda();
            } else if (cube.getMovimientosOrdena().get(i).equalsIgnoreCase("Eje Izquierda") || cube.getMovimientosOrdena().get(i).equalsIgnoreCase("eI")) {
                cube.ejeDerecha();
            } else if (cube.getMovimientosOrdena().get(i).equalsIgnoreCase("U")) {
                cube.UpPrima();
            } else if (cube.getMovimientosOrdena().get(i).equalsIgnoreCase("U'")) {
                cube.Up();
            } else if (cube.getMovimientosOrdena().get(i).equalsIgnoreCase("D")) {
                cube.DownPrima();
            } else if (cube.getMovimientosOrdena().get(i).equalsIgnoreCase("D'")) {
                cube.Down();
            } else if (cube.getMovimientosOrdena().get(i).equalsIgnoreCase("F")) {
                cube.FrontPrima();
            } else if (cube.getMovimientosOrdena().get(i).equalsIgnoreCase("F'")) {
                cube.Front();
            } else if (cube.getMovimientosOrdena().get(i).equalsIgnoreCase("B")) {
                cube.BackPrima();
            } else if (cube.getMovimientosOrdena().get(i).equalsIgnoreCase("B'")) {
                cube.Back();
            } else if (cube.getMovimientosOrdena().get(i).equalsIgnoreCase("R")) {
                cube.RigthPrima();
            } else if (cube.getMovimientosOrdena().get(i).equalsIgnoreCase("R'")) {
                cube.Rigth();
            } else if (cube.getMovimientosOrdena().get(i).equalsIgnoreCase("L")) {
                cube.LeftPrima();
            } else if (cube.getMovimientosOrdena().get(i).equalsIgnoreCase("L'")) {
                cube.Left();
            } else {
                System.out.println("Movimiento no definido: " + cube.getMovimientosOrdena().get(i));
            }
        }
        this.cube.getMovimientosOrdena().clear();
        this.cube.getOrdena().clear();
    }

    /**
     * Verifica si el cubo tiene todas las piezas
     *
     * @return true si está completo
     */
    public Boolean lleno() {
        Integer lleno = 0;
        String defecto = "-fx-border-color: #CFB53B;";
        for (int i = 0; i < 9; i++) {
            if (!this.frente.get(i).getStyle().equalsIgnoreCase(ColorLimpia.getStyle()) && !this.frente.get(i).getStyle().equalsIgnoreCase(defecto)) {
                lleno++;
            }
            if (!this.atras.get(i).getStyle().equalsIgnoreCase(ColorLimpia.getStyle()) && !this.atras.get(i).getStyle().equalsIgnoreCase(defecto)) {
                lleno++;
            }
            if (!this.izquierda.get(i).getStyle().equalsIgnoreCase(ColorLimpia.getStyle()) && !this.izquierda.get(i).getStyle().equalsIgnoreCase(defecto)) {
                lleno++;
            }
            if (!this.derecha.get(i).getStyle().equalsIgnoreCase(ColorLimpia.getStyle()) && !this.derecha.get(i).getStyle().equalsIgnoreCase(defecto)) {
                lleno++;
            }
            if (!this.arriba.get(i).getStyle().equalsIgnoreCase(ColorLimpia.getStyle()) && !this.arriba.get(i).getStyle().equalsIgnoreCase(defecto)) {
                lleno++;
            }
            if (!this.abajo.get(i).getStyle().equalsIgnoreCase(ColorLimpia.getStyle()) && !this.abajo.get(i).getStyle().equalsIgnoreCase(defecto)) {
                lleno++;
            }
        }
        return lleno == 54;
    }

    /**
     * Obtiene el color de un label
     *
     * @param label
     * @return color del label
     */
    public Color colorLabel(Label label) {
        return Color.valueOf(label.getStyle().substring(22, 29));
    }

    /**
     * Carga la matriz tridimensional con los datos del cubo visual
     */
    public void creaCuboLogico() {
        //Frente
        this.cube.getRubik()[0][0][0].setFrente(colorLabel(frente.get(0)));
        this.cube.getRubik()[0][1][0].setFrente(colorLabel(frente.get(1)));
        this.cube.getRubik()[0][2][0].setFrente(colorLabel(frente.get(2)));
        this.cube.getRubik()[1][0][0].setFrente(colorLabel(frente.get(3)));
        this.cube.getRubik()[1][1][0].setFrente(colorLabel(frente.get(4)));
        this.cube.getRubik()[1][2][0].setFrente(colorLabel(frente.get(5)));
        this.cube.getRubik()[2][0][0].setFrente(colorLabel(frente.get(6)));
        this.cube.getRubik()[2][1][0].setFrente(colorLabel(frente.get(7)));
        this.cube.getRubik()[2][2][0].setFrente(colorLabel(frente.get(8)));
        //Atras
        this.cube.getRubik()[0][0][2].setAtras(colorLabel(atras.get(2)));
        this.cube.getRubik()[0][1][2].setAtras(colorLabel(atras.get(1)));
        this.cube.getRubik()[0][2][2].setAtras(colorLabel(atras.get(0)));
        this.cube.getRubik()[1][0][2].setAtras(colorLabel(atras.get(5)));
        this.cube.getRubik()[1][1][2].setAtras(colorLabel(atras.get(4)));
        this.cube.getRubik()[1][2][2].setAtras(colorLabel(atras.get(3)));
        this.cube.getRubik()[2][0][2].setAtras(colorLabel(atras.get(8)));
        this.cube.getRubik()[2][1][2].setAtras(colorLabel(atras.get(7)));
        this.cube.getRubik()[2][2][2].setAtras(colorLabel(atras.get(6)));
        //Derecha
        this.cube.getRubik()[0][2][0].setDerecha(colorLabel(derecha.get(0)));
        this.cube.getRubik()[0][2][1].setDerecha(colorLabel(derecha.get(1)));
        this.cube.getRubik()[0][2][2].setDerecha(colorLabel(derecha.get(2)));
        this.cube.getRubik()[1][2][0].setDerecha(colorLabel(derecha.get(3)));
        this.cube.getRubik()[1][2][1].setDerecha(colorLabel(derecha.get(4)));
        this.cube.getRubik()[1][2][2].setDerecha(colorLabel(derecha.get(5)));
        this.cube.getRubik()[2][2][0].setDerecha(colorLabel(derecha.get(6)));
        this.cube.getRubik()[2][2][1].setDerecha(colorLabel(derecha.get(7)));
        this.cube.getRubik()[2][2][2].setDerecha(colorLabel(derecha.get(8)));
        //Izquierda
        this.cube.getRubik()[0][0][0].setIzquierda(colorLabel(izquierda.get(2)));
        this.cube.getRubik()[0][0][1].setIzquierda(colorLabel(izquierda.get(1)));
        this.cube.getRubik()[0][0][2].setIzquierda(colorLabel(izquierda.get(0)));
        this.cube.getRubik()[1][0][0].setIzquierda(colorLabel(izquierda.get(5)));
        this.cube.getRubik()[1][0][1].setIzquierda(colorLabel(izquierda.get(4)));
        this.cube.getRubik()[1][0][2].setIzquierda(colorLabel(izquierda.get(3)));
        this.cube.getRubik()[2][0][0].setIzquierda(colorLabel(izquierda.get(8)));
        this.cube.getRubik()[2][0][1].setIzquierda(colorLabel(izquierda.get(7)));
        this.cube.getRubik()[2][0][2].setIzquierda(colorLabel(izquierda.get(6)));
        //Arriba
        this.cube.getRubik()[0][0][0].setArriba(colorLabel(arriba.get(6)));
        this.cube.getRubik()[0][0][1].setArriba(colorLabel(arriba.get(3)));
        this.cube.getRubik()[0][0][2].setArriba(colorLabel(arriba.get(0)));
        this.cube.getRubik()[0][1][0].setArriba(colorLabel(arriba.get(7)));
        this.cube.getRubik()[0][1][1].setArriba(colorLabel(arriba.get(4)));
        this.cube.getRubik()[0][1][2].setArriba(colorLabel(arriba.get(1)));
        this.cube.getRubik()[0][2][0].setArriba(colorLabel(arriba.get(8)));
        this.cube.getRubik()[0][2][1].setArriba(colorLabel(arriba.get(5)));
        this.cube.getRubik()[0][2][2].setArriba(colorLabel(arriba.get(2)));
        //Abajo
        this.cube.getRubik()[2][0][0].setAbajo(colorLabel(abajo.get(0)));
        this.cube.getRubik()[2][0][1].setAbajo(colorLabel(abajo.get(3)));
        this.cube.getRubik()[2][0][2].setAbajo(colorLabel(abajo.get(6)));
        this.cube.getRubik()[2][1][0].setAbajo(colorLabel(abajo.get(1)));
        this.cube.getRubik()[2][1][1].setAbajo(colorLabel(abajo.get(4)));
        this.cube.getRubik()[2][1][2].setAbajo(colorLabel(abajo.get(7)));
        this.cube.getRubik()[2][2][0].setAbajo(colorLabel(abajo.get(2)));
        this.cube.getRubik()[2][2][1].setAbajo(colorLabel(abajo.get(5)));
        this.cube.getRubik()[2][2][2].setAbajo(colorLabel(abajo.get(8)));
    }

    /**
     * Carga datos de una partida anterior
     */
    public void cargaDatos() {
        Partida partida = (Partida) AppContext.getInstance().get("partida");
        int k = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (k < 9) {
                    this.frente.get(k).setStyle(partida.getCaraFrente()[i][j]+"\n-fx-border-color: #CFB53B;");
                    this.atras.get(k).setStyle(partida.getCaraAtras()[i][j]+"\n-fx-border-color: #CFB53B;");
                    this.derecha.get(k).setStyle(partida.getCaraDerecha()[i][j]+"\n-fx-border-color: #CFB53B;");
                    this.izquierda.get(k).setStyle(partida.getCaraIzquierda()[i][j]+"\n-fx-border-color: #CFB53B;");
                    this.arriba.get(k).setStyle(partida.getCaraArriba()[i][j]+"\n-fx-border-color: #CFB53B;");
                    this.abajo.get(k).setStyle(partida.getCaraAbajo()[i][j]+"\n-fx-border-color: #CFB53B;");
                }
                k++;
            }
        }
        this.izquierda.get(4).setText("L");
        this.izquierda.get(4).setStyle(this.izquierda.get(4).getStyle() + "-fx-font-size: 15px;" + "-fx-font-weight: bold;" + "-fx-font-family: 'Bangers', cursive;"
                + "\n-fx-text-fill: #CFB53B;" + "-fx-alignment:center;");
        this.derecha.get(4).setText("R");
        this.derecha.get(4).setStyle(this.derecha.get(4).getStyle() + "-fx-font-size: 15px;" + "-fx-font-weight: bold;" + "-fx-font-family: 'Bangers', cursive;"
                + "\n-fx-text-fill: #CFB53B;" + "-fx-alignment:center;");
        this.arriba.get(4).setText("U");
        this.arriba.get(4).setStyle(this.arriba.get(4).getStyle() + "-fx-font-size: 15px;" + "-fx-font-weight: bold;" + "-fx-font-family: 'Bangers', cursive;"
                + "\n-fx-text-fill: #CFB53B;" + "-fx-alignment:center;");
        this.abajo.get(4).setText("D");
        this.abajo.get(4).setStyle(this.abajo.get(4).getStyle() + "-fx-font-size: 15px;" + "-fx-font-weight: bold;" + "-fx-font-family: 'Bangers', cursive;"
                + "\n-fx-text-fill: #CFB53B;" + "-fx-alignment:center;");
        this.frente.get(4).setText("F");
        this.frente.get(4).setStyle(this.frente.get(4).getStyle() + "-fx-font-size: 15px;" + "-fx-font-weight: bold;" + "-fx-font-family: 'Bangers', cursive;"
                + "\n-fx-text-fill: #CFB53B;" + "-fx-alignment:center;");
        this.atras.get(4).setText("B");
        this.atras.get(4).setStyle(this.atras.get(4).getStyle() + "-fx-font-size: 15px;" + "-fx-font-weight: bold;" + "-fx-font-family: 'Bangers', cursive;"
                + "\n-fx-text-fill: #CFB53B;" + "-fx-alignment:center;");
    }

    /**
     * Vuelve a la pantalla de configuraciones sin aplicar cambios
     *
     * @param event
     */
    @FXML
    private void atras(ActionEvent event) {
        FlowController.getInstance().delete("ElegirPosiciones");
        FlowController.getInstance().goView("Configuraciones");
    }
}
