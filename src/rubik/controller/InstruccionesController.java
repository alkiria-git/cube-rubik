/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rubik.controller;

import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author 
 */
public class InstruccionesController extends Controller implements Initializable {

    @FXML
    private JFXButton btnAnterior;
    @FXML
    private JFXButton btnSiguiente;
    @FXML
    private AnchorPane apPagina1;
    @FXML
    private AnchorPane apPagina2;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        inicializa();
    }    

    @Override
    public void initialize() {
        this.btnAnterior.setVisible(false);
        this.btnSiguiente.setVisible(true);
    }
    
    /**
     * Inicializa componentes y atributos
     */
    public void inicializa() {
        this.btnAnterior.setVisible(false);
        this.btnSiguiente.setVisible(true);
        this.apPagina1.setVisible(true);
        this.apPagina2.setVisible(false);
    }

    /**
     * Muestra la pantalla anterior
     * @param event 
     */
    @FXML
    private void anterior(ActionEvent event) {
        this.btnSiguiente.setVisible(true);
        this.btnAnterior.setVisible(false);
        this.apPagina1.setVisible(true);
        this.apPagina2.setVisible(false);
    }

    /**
     * Muestra la pantalla siguiente
     * @param event 
     */
    @FXML
    private void siguiente(ActionEvent event) {
        this.btnAnterior.setVisible(true);
        this.btnSiguiente.setVisible(false);
        this.apPagina1.setVisible(false);
        this.apPagina2.setVisible(true);
    }
   
    
}
