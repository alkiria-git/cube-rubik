/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rubik.controller;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import rubik.util.AppContext;

/**
 * FXML Controller class
 *
 * @author 
 */
public class FinalizarController extends Controller implements Initializable {

    @FXML
    private Label lblEstadoJuego;
    @FXML
    private HBox hbJugadores;
    @FXML
    private Label lblGano1;

    private JuegoController jc;
    private DrawerController drawer;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        inicializa();
    }

    @Override
    public void initialize() {
        tiempo();
    }

    public void inicializa() {
        this.jc = (JuegoController) AppContext.getInstance().get("jc");
        this.drawer = (DrawerController) AppContext.getInstance().get("drawer");
        tiempo();
    }
    
    public void tiempo() {
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            int second = 3;

            @Override
            public void run() {
                Platform.runLater(() -> {
                    second--;
                    if (second < 0) {
                        timer.cancel();
                        drawer.tablaPosiciones();
                    }
                });
            }
        };
        timer.schedule(task, 0, 1000);
    }
}
