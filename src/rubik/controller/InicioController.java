/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rubik.controller;

import com.jfoenix.controls.JFXDrawer;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import rubik.util.AppContext;

/**
 * FXML Controller class
 *
 * @author 
 */
public class InicioController extends Controller implements Initializable {

    @FXML
    private ImageView hMenu;
    @FXML
    private JFXDrawer dItemsMenu;
    @FXML
    private HBox apLogo;
    @FXML
    private VBox vbRoot;
    
    //Atributos
    private Double posX;
    private Double posY;
    @FXML
    private BorderPane apWrapper;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.inicializa();
        abrirMenu();
    }    

    @Override
    public void initialize() {
        
    }
    
    /**
     * Inicializa atributos y componentes
     */
    public void inicializa(){
        //Atributos
        this.posX = 0.0;
        this.posY = 0.0;
        
        //Metodos
        this.arrastrar();
        //this.hamMenu();
        
        //Statico
        AppContext.getInstance().set("ic", this);
    }
    
    /**
     * Arrastra la aplicación
     */
    public void arrastrar(){
        this.apLogo.setOnMousePressed((MouseEvent event) -> {
            this.posX = event.getSceneX();
            this.posY = event.getSceneY();
        });

        this.apLogo.setOnMouseDragged((MouseEvent event) -> {
            ((Stage) apLogo.getScene().getWindow()).setX(event.getScreenX() - this.posX);
            ((Stage) apLogo.getScene().getWindow()).setY(event.getScreenY() - this.posY);
        });
    }
    
    public VBox getVbRoot(){
        return vbRoot;
    }
    
    public ImageView gethMenu() {
        return hMenu;
    }
    
    /**
     * Abre el menu
     */
    public void abrirMenu(){
        try {
            HBox hb = FXMLLoader.load(getClass().getResource("/rubik/view/Drawer.fxml"));
            this.dItemsMenu.setSidePane(hb);
        } catch (IOException ex) {
            Logger.getLogger(InicioController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        this.hMenu.setOnMouseClicked((MouseEvent e)->{
            if (this.dItemsMenu.isShown()) {
                this.dItemsMenu.close();
            } else {
                this.dItemsMenu.open();
            }
            e.consume();
        });
        
    }
}
