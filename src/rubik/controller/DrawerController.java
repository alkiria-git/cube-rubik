/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rubik.controller;

import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.Stage;
import rubik.util.AppContext;
import rubik.util.FlowController;

/**
 * FXML Controller class
 *
 * @author
 */
public class DrawerController extends Controller implements Initializable {

    @FXML
    private JFXButton btnJuego;
    @FXML
    private JFXButton btnConfiguracionJuego;
    @FXML
    private JFXButton btnTablaPosiciones;
    @FXML
    private JFXButton btnAcercaDe;
    @FXML
    private JFXButton btnSalir;
    @FXML
    private JFXButton btnIntrucciones;

    private InicioController ic;
    private Boolean visible;
    private JuegoController jc;
    private Boolean confi;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        inicializa();
    }

    @Override
    public void initialize() {
    }

    /**
     * Inicializa los componentes y variables
     */
    public void inicializa() {
        AppContext.getInstance().set("drawer", this);
        //Atributos
        this.ic = (InicioController) AppContext.getInstance().get("ic");
        this.jc = (JuegoController) AppContext.getInstance().get("jc");
        this.visible = false;
//        this.confi = false;
//        bloqueaPantallas();
        this.btnIntrucciones.setDisable(false);
    }

    /**
     * Muestra la pantalla de juego
     */
    @FXML
    public void juego() {
        FlowController.getInstance().delete("Juego");
        Timer tim = (Timer) AppContext.getInstance().get("timer");
        if (tim != null) {
            tim.cancel();
        }
        if (this.visible) {
            FlowController.getInstance().goView("Vacio");
            desbloqueaPantallas();
            this.visible = false;
        } else {
            FlowController.getInstance().goView("Juego");
            bloqueaPantallas();
            this.btnJuego.setDisable(false);
            this.visible = true;
            this.confi = true;
        }

    }

    /**
     * Muestra la pantalla de configuraciones
     */
    @FXML
    public void configuraciones() {
        if (this.visible) {
            FlowController.getInstance().delete("Configuraciones");
            FlowController.getInstance().goView("Vacio");
            desbloqueaPantallas();
            this.visible = false;
        } else {
            FlowController.getInstance().goView("Configuraciones");
            bloqueaPantallas();
            this.btnConfiguracionJuego.setDisable(false);
            this.visible = true;
        }
    }
    
    /**
     * Muestra la pantalla de la tabla de posiciones
     */
    @FXML
    public void tablaPosiciones() {
        FlowController.getInstance().delete("Tabla");
        if (this.visible) {
            FlowController.getInstance().goView("Vacio");
            desbloqueaPantallas();
            this.visible = false;
        } else {
            FlowController.getInstance().goView("Tabla");
            bloqueaPantallas();
            this.btnTablaPosiciones.setDisable(false);
            this.visible = true;
        }
    }

    /**
     * Muestra la pantalla del acerca de
     * @param event 
     */
    @FXML
    private void acercaDe(ActionEvent event) {
        if (this.visible) {
            FlowController.getInstance().goView("Vacio");
            desbloqueaPantallas();
            this.visible = false;
        } else {
            FlowController.getInstance().goView("AcercaDe");
            bloqueaPantallas();
            this.btnAcercaDe.setDisable(false);
            this.visible = true;
        }
    }

    /**
     * Muestra la pantalla de las instrucciones
     * @param event 
     */
    @FXML
    private void instrucciones(ActionEvent event) {
        if (this.visible) {
            FlowController.getInstance().goView("Vacio");
            desbloqueaPantallas();
            this.visible = false;
        } else {
            FlowController.getInstance().goView("Instrucciones");
            bloqueaPantallas();
            this.btnIntrucciones.setDisable(false);
            this.visible = true;
        }
    }

    /**
     * Cierra la aplicación
     * @param event 
     */
    @FXML
    private void salir(ActionEvent event) {
        Timer tim = (Timer) AppContext.getInstance().get("timer");
        if (tim != null) {
            tim.cancel();
        }
        ((Stage) this.btnAcercaDe.getScene().getWindow()).close();
    }

    /**
     * Muestra la pantalla de finalizar
     */
    public void finalizar() {
        FlowController.getInstance().goView("Finalizar");
        bloqueaPantallas();
        this.btnTablaPosiciones.setDisable(false);
    }

    /**
     * Bloquea los botones de acceso a las pantallas para evitar conflictos
     */
    public void bloqueaPantallas() {
        this.btnAcercaDe.setDisable(true);
        this.btnConfiguracionJuego.setDisable(true);
        this.btnJuego.setDisable(true);
        this.btnTablaPosiciones.setDisable(true);
        this.btnIntrucciones.setDisable(true);
    }

    /**
     * Desbloquea las pantallas para el acceso a las diferentes funcionalidades
     */
    public void desbloqueaPantallas() {
        this.btnAcercaDe.setDisable(false);
        this.btnConfiguracionJuego.setDisable(false);
        this.btnJuego.setDisable(false);
        this.btnTablaPosiciones.setDisable(false);
        this.btnIntrucciones.setDisable(false);

    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

}
